﻿Imports System.Data.SqlClient
Imports System.IO
Public Class frmLoginForm
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            p_username = Me.txtUserName.Text
            p_password = Me.txtPassword.Text
            p_dbname = Me.txtDatabase.Text.Trim
            p_servername = Me.txtServer.Text.Trim

            Dim ConnStr As String = _
                                 "Data Source=" & Me.txtServer.Text.Trim & _
                                 ";Initial Catalog=" & Me.txtDatabase.Text.Trim & _
                                 ";User ID=" & p_username & ";" & _
                                 "password = " & p_password & ";" & _
                                 "Connection Timeout=0"
            Dim ConnStr1 As String = _
                 "Data Source=" & p_servername & ";Initial Catalog= EMISDB ;User ID=" & p_username & ";" & "password = " & p_password & ";" & "Connection Timeout=0"
            sqlconn = New SqlClient.SqlConnection(ConnStr)
            sqlconn.Open()
            If sqlconn.State = ConnectionState.Open Then
                p_ConnStr = ConnStr
                p_ConnStr1 = ConnStr1
                logindialogflag = 1

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim objStreamWriter As StreamWriter
                'Pass the file path and the file name to the StreamWriter constructor.
                objStreamWriter = New StreamWriter("C:\EMIS_SETTINGS\db.txt")
                'Write a line of text.
                objStreamWriter.WriteLine(Me.txtDatabase.Text.Trim & ";" & Me.txtServer.Text.Trim)
                'Close the file.
                objStreamWriter.Close()
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Else
                MsgBox("Invalid Login", MsgBoxStyle.Information)
                Me.txtUserName.Focus()
                logindialogflag = 0
            End If
            sqlconn.Close()
            Me.Close()
        Catch err As Exception
            logindialogflag = 0
            MsgBox(err.Message)
        End Try
    End Sub
    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        logindialogflag = 0
        Me.Close()
    End Sub

    Private Sub frmLoginForm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If logindialogflag = 0 Then
            MDIMERGE.Close()
        End If
    End Sub
    Private Sub LoginForm1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If File.Exists("C:\EMIS_SETTINGS\db.txt") Then
            Dim ioFile As New StreamReader("C:\EMIS_SETTINGS\db.txt")
            Dim ioLine As String ' Going to hold one line at a time
            Dim ioLines As String ' Going to hold whole file
            ioLine = ioFile.ReadLine
            ioLines = ioLine
            While Not ioLine = ""
                ioLine = ioFile.ReadLine
                ioLines = ioLines & vbCrLf & ioLine
            End While
            Dim i As Integer = 0
            i = ioLines.IndexOf(";")
            If i > 0 Then
                p_dbname = Mid(ioLines, 1, i).Trim
                p_servername = Mid(ioLines, i + 2, ioLines.Length - i + 1).Trim
            End If
            Me.txtDatabase.Text = p_dbname.ToUpper
            Me.txtServer.Text = p_servername.ToUpper
            ioFile.Close()
        Else
            Dim TextFile As New StreamWriter("C:\EMIS_SETTINGS\db.txt")
            TextFile.WriteLine("Server1;Db1")
            TextFile.Close()
        End If
    End Sub
End Class
