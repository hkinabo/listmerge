﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptParameters
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdRegionCand = New System.Windows.Forms.RadioButton()
        Me.rdcandrange = New System.Windows.Forms.RadioButton()
        Me.rdAll = New System.Windows.Forms.RadioButton()
        Me.rdCentre = New System.Windows.Forms.RadioButton()
        Me.rdDistrict = New System.Windows.Forms.RadioButton()
        Me.rdRegion = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnBatch = New System.Windows.Forms.Button()
        Me.Gpcandidate = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCandidateTo = New System.Windows.Forms.MaskedTextBox()
        Me.txtCandidateFrom = New System.Windows.Forms.MaskedTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cmbCentre = New System.Windows.Forms.ComboBox()
        Me.cmbDistrict = New System.Windows.Forms.ComboBox()
        Me.cmbRegion = New System.Windows.Forms.ComboBox()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnListGenerate = New System.Windows.Forms.Button()
        Me.sfdText = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Gpcandidate.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdRegionCand)
        Me.GroupBox1.Controls.Add(Me.rdcandrange)
        Me.GroupBox1.Controls.Add(Me.rdAll)
        Me.GroupBox1.Controls.Add(Me.rdCentre)
        Me.GroupBox1.Controls.Add(Me.rdDistrict)
        Me.GroupBox1.Controls.Add(Me.rdRegion)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox1.Size = New System.Drawing.Size(567, 112)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Printing Options"
        '
        'rdRegionCand
        '
        Me.rdRegionCand.AutoSize = True
        Me.rdRegionCand.Location = New System.Drawing.Point(265, 81)
        Me.rdRegionCand.Margin = New System.Windows.Forms.Padding(4)
        Me.rdRegionCand.Name = "rdRegionCand"
        Me.rdRegionCand.Size = New System.Drawing.Size(255, 21)
        Me.rdRegionCand.TabIndex = 7
        Me.rdRegionCand.TabStop = True
        Me.rdRegionCand.Text = "By Region and  Candidates Number"
        Me.rdRegionCand.UseVisualStyleBackColor = True
        '
        'rdcandrange
        '
        Me.rdcandrange.AutoSize = True
        Me.rdcandrange.Location = New System.Drawing.Point(265, 53)
        Me.rdcandrange.Margin = New System.Windows.Forms.Padding(4)
        Me.rdcandrange.Name = "rdcandrange"
        Me.rdcandrange.Size = New System.Drawing.Size(181, 21)
        Me.rdcandrange.TabIndex = 6
        Me.rdcandrange.TabStop = True
        Me.rdcandrange.Text = "By Candidates Numbers"
        Me.rdcandrange.UseVisualStyleBackColor = True
        '
        'rdAll
        '
        Me.rdAll.AutoSize = True
        Me.rdAll.Location = New System.Drawing.Point(132, 25)
        Me.rdAll.Margin = New System.Windows.Forms.Padding(4)
        Me.rdAll.Name = "rdAll"
        Me.rdAll.Size = New System.Drawing.Size(44, 21)
        Me.rdAll.TabIndex = 5
        Me.rdAll.TabStop = True
        Me.rdAll.Text = "All"
        Me.rdAll.UseVisualStyleBackColor = True
        '
        'rdCentre
        '
        Me.rdCentre.AutoSize = True
        Me.rdCentre.Location = New System.Drawing.Point(265, 25)
        Me.rdCentre.Margin = New System.Windows.Forms.Padding(4)
        Me.rdCentre.Name = "rdCentre"
        Me.rdCentre.Size = New System.Drawing.Size(91, 21)
        Me.rdCentre.TabIndex = 4
        Me.rdCentre.TabStop = True
        Me.rdCentre.Text = "By Centre"
        Me.rdCentre.UseVisualStyleBackColor = True
        '
        'rdDistrict
        '
        Me.rdDistrict.AutoSize = True
        Me.rdDistrict.Location = New System.Drawing.Point(135, 81)
        Me.rdDistrict.Margin = New System.Windows.Forms.Padding(4)
        Me.rdDistrict.Name = "rdDistrict"
        Me.rdDistrict.Size = New System.Drawing.Size(92, 21)
        Me.rdDistrict.TabIndex = 3
        Me.rdDistrict.TabStop = True
        Me.rdDistrict.Text = "By District"
        Me.rdDistrict.UseVisualStyleBackColor = True
        '
        'rdRegion
        '
        Me.rdRegion.AutoSize = True
        Me.rdRegion.Location = New System.Drawing.Point(132, 53)
        Me.rdRegion.Margin = New System.Windows.Forms.Padding(4)
        Me.rdRegion.Name = "rdRegion"
        Me.rdRegion.Size = New System.Drawing.Size(94, 21)
        Me.rdRegion.TabIndex = 2
        Me.rdRegion.TabStop = True
        Me.rdRegion.Text = "By Region"
        Me.rdRegion.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnBatch)
        Me.GroupBox2.Controls.Add(Me.Gpcandidate)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.cmbCentre)
        Me.GroupBox2.Controls.Add(Me.cmbDistrict)
        Me.GroupBox2.Controls.Add(Me.cmbRegion)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 134)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
        Me.GroupBox2.Size = New System.Drawing.Size(567, 223)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'btnBatch
        '
        Me.btnBatch.Location = New System.Drawing.Point(396, 31)
        Me.btnBatch.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBatch.Name = "btnBatch"
        Me.btnBatch.Size = New System.Drawing.Size(147, 31)
        Me.btnBatch.TabIndex = 13
        Me.btnBatch.Text = "NER Control Sheet"
        Me.btnBatch.UseVisualStyleBackColor = True
        Me.btnBatch.Visible = False
        '
        'Gpcandidate
        '
        Me.Gpcandidate.Controls.Add(Me.Label6)
        Me.Gpcandidate.Controls.Add(Me.Label5)
        Me.Gpcandidate.Controls.Add(Me.txtCandidateTo)
        Me.Gpcandidate.Controls.Add(Me.txtCandidateFrom)
        Me.Gpcandidate.Location = New System.Drawing.Point(4, 138)
        Me.Gpcandidate.Margin = New System.Windows.Forms.Padding(4)
        Me.Gpcandidate.Name = "Gpcandidate"
        Me.Gpcandidate.Padding = New System.Windows.Forms.Padding(4)
        Me.Gpcandidate.Size = New System.Drawing.Size(405, 50)
        Me.Gpcandidate.TabIndex = 12
        Me.Gpcandidate.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(228, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 17)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "To:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 20)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 17)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "From Candidate:"
        '
        'txtCandidateTo
        '
        Me.txtCandidateTo.Location = New System.Drawing.Point(261, 16)
        Me.txtCandidateTo.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCandidateTo.Mask = "?0000-0000"
        Me.txtCandidateTo.Name = "txtCandidateTo"
        Me.txtCandidateTo.Size = New System.Drawing.Size(91, 22)
        Me.txtCandidateTo.TabIndex = 9
        '
        'txtCandidateFrom
        '
        Me.txtCandidateFrom.Location = New System.Drawing.Point(128, 16)
        Me.txtCandidateFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.txtCandidateFrom.Mask = "?0000-0000"
        Me.txtCandidateFrom.Name = "txtCandidateFrom"
        Me.txtCandidateFrom.Size = New System.Drawing.Size(92, 22)
        Me.txtCandidateFrom.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(69, 113)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 17)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Centre:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(68, 80)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 17)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "District:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(65, 38)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 17)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Region:"
        '
        'cmbCentre
        '
        Me.cmbCentre.FormattingEnabled = True
        Me.cmbCentre.Location = New System.Drawing.Point(132, 110)
        Me.cmbCentre.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbCentre.Name = "cmbCentre"
        Me.cmbCentre.Size = New System.Drawing.Size(409, 24)
        Me.cmbCentre.TabIndex = 3
        '
        'cmbDistrict
        '
        Me.cmbDistrict.FormattingEnabled = True
        Me.cmbDistrict.Location = New System.Drawing.Point(132, 76)
        Me.cmbDistrict.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbDistrict.Name = "cmbDistrict"
        Me.cmbDistrict.Size = New System.Drawing.Size(409, 24)
        Me.cmbDistrict.TabIndex = 2
        '
        'cmbRegion
        '
        Me.cmbRegion.FormattingEnabled = True
        Me.cmbRegion.Location = New System.Drawing.Point(132, 34)
        Me.cmbRegion.Margin = New System.Windows.Forms.Padding(4)
        Me.cmbRegion.Name = "cmbRegion"
        Me.cmbRegion.Size = New System.Drawing.Size(255, 24)
        Me.cmbRegion.TabIndex = 1
        '
        'btnPreview
        '
        Me.btnPreview.Location = New System.Drawing.Point(289, 366)
        Me.btnPreview.Margin = New System.Windows.Forms.Padding(4)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(123, 31)
        Me.btnPreview.TabIndex = 2
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(419, 366)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(123, 31)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnListGenerate
        '
        Me.btnListGenerate.Location = New System.Drawing.Point(158, 366)
        Me.btnListGenerate.Name = "btnListGenerate"
        Me.btnListGenerate.Size = New System.Drawing.Size(124, 31)
        Me.btnListGenerate.TabIndex = 4
        Me.btnListGenerate.Text = "Button1"
        Me.btnListGenerate.UseVisualStyleBackColor = True
        Me.btnListGenerate.Visible = False
        '
        'frmRptParameters
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(612, 414)
        Me.Controls.Add(Me.btnListGenerate)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnPreview)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmRptParameters"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmRptParameters"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Gpcandidate.ResumeLayout(False)
        Me.Gpcandidate.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdCentre As System.Windows.Forms.RadioButton
    Friend WithEvents rdDistrict As System.Windows.Forms.RadioButton
    Friend WithEvents rdRegion As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCentre As System.Windows.Forms.ComboBox
    Friend WithEvents cmbDistrict As System.Windows.Forms.ComboBox
    Friend WithEvents cmbRegion As System.Windows.Forms.ComboBox
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents rdAll As System.Windows.Forms.RadioButton
    Friend WithEvents rdcandrange As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCandidateTo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtCandidateFrom As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Gpcandidate As System.Windows.Forms.GroupBox
    Friend WithEvents btnBatch As System.Windows.Forms.Button
    Friend WithEvents rdRegionCand As System.Windows.Forms.RadioButton
    Friend WithEvents btnListGenerate As System.Windows.Forms.Button
    Friend WithEvents sfdText As System.Windows.Forms.SaveFileDialog
End Class
