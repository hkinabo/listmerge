﻿Imports System.IO
Public Class UploadCSV
    Public Class CurrentState
        Public FileCount As Integer
        Public CurrentFile As String
        Public CurrentFileNo As Integer
        Public CurrentRecord As String
        Public RecordCount As Integer
        Public pct As Integer
        Public str(6) As String
        Public err As String
    End Class

    Public FilePath As String

    Public Sub UploadCandidates(ByVal worker As System.ComponentModel.BackgroundWorker,
                              ByVal e As System.ComponentModel.DoWorkEventArgs)
        If FilePath = "" Then
            MsgBox("You must Browse the folder")
            Exit Sub
        Else
            Dim di As DirectoryInfo = New DirectoryInfo(FilePath)
            Dim files As FileInfo() = di.GetFiles("*.csv", SearchOption.AllDirectories)
            Dim CurrentFileNo As Integer = 0

            'ERROR LOG FILE
            Dim TextFile As New StreamWriter(FilePath & "\ERROR.txt")

            For Each File As FileInfo In files
                Dim str As String
                Dim indx As Integer = 0
                Dim CurrentRecord As String = ""
                Dim CurentFile As String = ""
                Dim lineCount As Integer = 0
                Dim filepth As String = File.FullName
                'Dim filepth = "C:\Users\Admin\Desktop\mtika\1_S2511_80_FTSEE 2014- USAJILI WA WATAHINIWA BAHI, CHAMWINO, DODOMA.csv"
                Dim sr As StreamReader = New StreamReader(filepth, True)
                Dim state As New CurrentState
                Dim pct As Integer = 0
                Dim strary() As String
                Dim filecount = files.Count

                Dim CentreNo As String = ""
                Dim CentreName As String = ""
                Dim CandidateNo As String = ""
                Dim FirstName As String = ""
                Dim MiddleName As String = ""
                Dim LastName As String = ""
                Dim Sex As String = ""
                Dim Repeater As String = ""

                lineCount = IO.File.ReadAllLines(filepth).Length

                state.FileCount = filecount
                state.CurrentFileNo = CurrentFileNo + 1

                Do While sr.Peek() >= 0
                    Try
                        str = sr.ReadLine()
                        strary = str.Split(",")

                        'Geting current Centre
                        CurentFile = Trim(strary(1)) + " : " + Trim(strary(2))
                        'Geting Current Candidate
                        CurrentRecord = Trim(strary(3)) + " : " + Trim(strary(4)) + " " + Trim(strary(5)) + " " + Trim(strary(6))

                        Dim ZoneCode As String = Trim(strary(0)).ToString
                        If ZoneCode.Length = 1 Then
                            ZoneCode = "0" + ZoneCode
                        End If

                        CentreNo = Trim(strary(1)).ToString
                        CentreName = Trim(strary(2)).ToString
                        CandidateNo = Trim(strary(3)).ToString
                        FirstName = Trim(strary(4)).ToString
                        MiddleName = Trim(strary(5)).ToString
                        LastName = Trim(strary(6)).ToString
                        Sex = Trim(strary(7)).ToString
                        Repeater = Trim(strary(8)).ToString

                        ''Retrieve candidates registered subject into Array
                        Dim arraylength As Integer = 0
                        arraylength = strary.Count - 10
                        Dim RegisteredSubject(arraylength) As String

                        For sbj As Integer = 0 To (arraylength)
                            RegisteredSubject(sbj) = Trim(strary(sbj + 9)).ToString
                        Next

                        'Inserting Centre
                        If indx = 0 Then
                            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
                            sqlconn.Open()
                            sqlcmd = sqlconn.CreateCommand
                            With sqlcmd
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                .CommandText = "ManipulateCentre"
                                .Parameters.AddWithValue("@CentreNo", CentreNo)
                                .Parameters.AddWithValue("@CentreName", CentreName)
                                .Parameters.AddWithValue("@ZoneCode", ZoneCode)
                                .ExecuteNonQuery()
                            End With
                            sqlcmd.Parameters.Clear()
                            sqlconn.Close()
                        End If

                        'Inserting(Candidates)
                        sqlconn = New SqlClient.SqlConnection(p_ConnStr)
                        sqlconn.Open()
                        sqlcmd = sqlconn.CreateCommand
                        With sqlcmd
                            .CommandType = CommandType.StoredProcedure
                            .CommandTimeout = 0
                            .CommandText = "ManipulateCandidate"
                            .Parameters.AddWithValue("@CandidateNo", CandidateNo)
                            .Parameters.AddWithValue("@FirstName", FirstName)
                            .Parameters.AddWithValue("@MiddleName", MiddleName)
                            .Parameters.AddWithValue("@SurName", LastName)
                            .Parameters.AddWithValue("@Sex", Sex)
                            .Parameters.AddWithValue("@CentreNo", CentreNo)
                            .Parameters.AddWithValue("@Repeater", Repeater)
                            .Parameters.AddWithValue("@ZoneCode", ZoneCode)
                            .ExecuteNonQuery()
                        End With
                        sqlcmd.Parameters.Clear()
                        sqlconn.Close()

                        'Registering candidates Subjects
                        For Each subj As String In RegisteredSubject
                            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
                            sqlconn.Open()
                            sqlcmd = sqlconn.CreateCommand
                            With sqlcmd
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                .CommandText = "ManipulateCandidateMark"
                                .Parameters.AddWithValue("@CandidateNo", CandidateNo)
                                .Parameters.AddWithValue("@CentreNo", CentreNo)
                                .Parameters.AddWithValue("@SubjectCode", subj.ToString)
                                .Parameters.AddWithValue("@ZoneCode", ZoneCode)
                                .ExecuteNonQuery()
                            End With
                            sqlcmd.Parameters.Clear()
                            sqlconn.Close()
                        Next

                        state.str(0) = CentreNo
                        state.str(1) = CentreName
                        state.str(2) = CandidateNo
                        state.str(3) = Sex
                        state.str(4) = Repeater
                        state.str(5) = FirstName & " " & MiddleName & " " & LastName

                    Catch ed As Exception
                        CurrentRecord = ""
                        state.CurrentRecord = "Failed Importing File"
                        state.err = ed.Message
                        e.Cancel = True
                        'Exit Sub
                        TextFile.WriteLine(CentreNo & "-" & CentreName & "-" & CandidateNo & "-" & ed.Message)
                    End Try
                    indx = indx + 1
                    pct = ((indx) / lineCount) * 100
                    state.pct = pct

                    state.CurrentRecord = CurrentRecord
                    state.CurrentFile = CurentFile
                    state.RecordCount = lineCount
                    worker.ReportProgress(0, state)

                Loop
                sr.Close()
                CurrentFileNo = CurrentFileNo + 1
            Next
            TextFile.Close()
        End If
    End Sub
End Class
