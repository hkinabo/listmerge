﻿Imports System.IO
Imports System.Data.SqlClient

Public Class UploadMark
    Public Class CurrentState
        Public FileCount As Integer
        Public CurrentFile As String
        Public CurrentFileNo As Integer
        Public CurrentRecord As String
        Public RecordCount As Integer
        Public pct As Integer
    End Class

    Public FilePath As String

    Public Sub UploadNewMarks(ByVal worker As System.ComponentModel.BackgroundWorker,
                              ByVal e As System.ComponentModel.DoWorkEventArgs)
        If FilePath = "" Then
            MsgBox("You must Browse the folder")
            Exit Sub
        Else
            Dim di As DirectoryInfo = New DirectoryInfo(FilePath)
            Dim files As FileInfo() = di.GetFiles("*.csv", SearchOption.AllDirectories)
            Dim CurrentFileNo As Integer = 0

            'ERROR LOG FILE
            ' Dim TextFile As New StreamWriter(FilePath & "\ERROR.txt")

            Dim destDirectory As String = FilePath & "\" & "ERRDIR"
            If Not System.IO.Directory.Exists(destDirectory) Then
                System.IO.Directory.CreateDirectory(destDirectory)
            End If

            For Each File As FileInfo In files
                Dim str As String
                Dim indx As Integer = 0
                Dim CurrentRecord As String
                Dim lineCount As Integer
                Dim filepth As String = File.FullName
                Dim sr As StreamReader = New StreamReader(filepth, True)
                Dim state As New CurrentState
                Dim pct As Integer
                Dim strary() As String
                Dim CurentFile As String = ""
                Dim RegionNumber As String = ""
                Dim DistrictNumber As String = ""
                Dim CentreNumber As String = ""
                Dim CandidateNumber As String = ""
                Dim SubjectCode As String = ""
                Dim SexCode As String = ""
                Dim Attendance As String = ""
                Dim Abc As String = ""
                Dim TotalMark As String = ""
                Dim Item As String = ""
                Dim Item1 As String = ""
                Dim Item2 As String = ""
                Dim Item3 As String = ""
                Dim Item4 As String = ""
                Dim Item5 As String = ""
                Dim filecount = files.Count
                Dim StatusCode As String = ""

                Dim filename As String = File.Name
                Dim sourceDirectory As String = File.DirectoryName

                Dim Not_registered_candidatesError As System.IO.FileInfo
                Dim Not_registered_subjectsError As System.IO.FileInfo
                Dim Not_registered_centresError As System.IO.FileInfo
                Dim Others_errorsError As System.IO.FileInfo
                Dim ReUploadFileError As System.IO.FileInfo

                'Dim fileerror As System.IO.FileInfo
                'Dim fileerror2 As System.IO.FileInfo

                'Dim TextFile As New StreamWriter(FilePath & "\" & Replace(filename, ".csv", "") & "ERROR.txt")
                Dim ReUploadFile As New StreamWriter(destDirectory & "\" & Replace(filename, ".csv", "") & "-ReUploadFile.csv")

                Dim Not_registered_candidates As New StreamWriter(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_candidates.txt")
                Dim Not_registered_subjects As New StreamWriter(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_subjects.txt")
                Dim Not_registered_centres As New StreamWriter(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_centres.txt")
                Dim Others_errors As New StreamWriter(FilePath & "\" & Replace(filename, ".csv", "") & "-Others_errors.txt")

                lineCount = IO.File.ReadAllLines(filepth).Length
                state.FileCount = filecount
                state.CurrentFileNo = CurrentFileNo + 1

                If pUplodBy = 1 Then  ' to load data from OMR
                    Do While sr.Peek() >= 0



                        Try
                            str = sr.ReadLine()
                            strary = str.Split(",")

                            'Geting current Centre

                            CurentFile = Trim(strary(0)) + " : " + Trim(strary(1))
                            'Geting Current Candidate
                            CurrentRecord = "Record No.  " + CStr(indx + 1) + " : " + Trim(strary(2)) + " : " + Trim(strary(4))


                            RegionNumber = strary(0).ToString
                            DistrictNumber = strary(1).ToString
                            CentreNumber = strary(2).ToString
                            CandidateNumber = strary(3).ToString
                            SubjectCode = strary(5).ToString
                            SexCode = strary(4).ToString
                            Attendance = strary(6).ToString
                            TotalMark = strary(7).ToString
                            Abc = strary(8).ToString
                            Item = strary(9).ToString

                            'Check if Centre is missing and Insert as New Centre

                            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
                            sqlconn.Open()
                            sqlcmd = sqlconn.CreateCommand
                            With sqlcmd
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                .CommandText = "[cs_ManipulateListOMR]"
                                .Parameters.AddWithValue("@RegionNumber", RegionNumber)
                                .Parameters.AddWithValue("@DistrictNumber", DistrictNumber)
                                .Parameters.AddWithValue("@CentreNumber", CentreNumber)
                                .Parameters.AddWithValue("@CandidateNumber", CandidateNumber)
                                .Parameters.AddWithValue("@SubjectCode", SubjectCode)
                                .Parameters.AddWithValue("@SexCode", SexCode)
                                .Parameters.AddWithValue("@TotalMark", TotalMark)
                                'MsgBox("*" & StatusCode & "*")
                                If Attendance = "V" Then
                                    .Parameters.AddWithValue("@Item", Item)
                                    .Parameters.AddWithValue("@Attendance", 1)
                                ElseIf Attendance = "A" Then
                                    .Parameters.AddWithValue("@Attendance", 0)
                                End If
                                .ExecuteNonQuery()
                            End With
                            sqlcmd.Parameters.Clear()
                            sqlconn.Close()
                        Catch ed As Exception
                            If ed.Message.Contains("Subject") Then
                                Not_registered_subjects.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)
                            ElseIf ed.Message.Contains("Candidate") Then
                                Not_registered_candidates.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)
                            ElseIf ed.Message.Contains("Centre") Then
                                Not_registered_centres.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)
                            Else
                                Others_errors.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item & "-" & ed.Message)
                            End If

                            ReUploadFile.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)

                            CurrentRecord = ""
                            state.CurrentRecord = "Failed"

                        End Try

                        pct = ((indx + 1) / lineCount) * 100
                        indx = indx + 1

                        'MsgBox(indx & "-" & lineCount & "-" & pct)
                        If pct > 100 Then pct = 100

                        state.pct = pct
                        state.CurrentRecord = CurrentRecord
                        state.CurrentFile = CurentFile
                        state.RecordCount = lineCount
                        worker.ReportProgress(0, state)
                    Loop

                ElseIf pUplodBy = 2 Then 'To load data from kituoni

                    Do While sr.Peek() >= 0



                        Try
                            str = sr.ReadLine()
                            strary = str.Split(",")

                            'Geting current Centre

                            CurentFile = Trim(strary(0)) + " : " + Trim(strary(1))
                            'Geting Current Candidate
                            CurrentRecord = "Record No.  " + CStr(indx + 1) + " : " + Trim(strary(2)) + " : " + Trim(strary(4))

                            RegionNumber = strary(0).ToString.Substring(0, 4)
                            DistrictNumber = strary(0).ToString.Substring(4, 2)
                            CentreNumber = strary(0).ToString.Substring(6, 3)
                            CandidateNumber = strary(0).ToString.Substring(10, 3)
                            SubjectCode = strary(1).ToString
                            'SexCode = strary(4).ToString
                            Attendance = strary(5).ToString
                            TotalMark = strary(11).ToString
                            'Abc = strary(9).ToString
                            Item1 = strary(6).ToString
                            Item2 = strary(7).ToString
                            Item3 = strary(8).ToString
                            Item4 = strary(9).ToString
                            Item5 = strary(10).ToString

                            'Check if Centre is missing and Insert as New Centre

                            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
                            sqlconn.Open()
                            sqlcmd = sqlconn.CreateCommand
                            With sqlcmd
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                .CommandText = "[cs_ManipulateListKituoni]"
                                .Parameters.AddWithValue("@RegionNumber", RegionNumber)
                                .Parameters.AddWithValue("@DistrictNumber", DistrictNumber)
                                .Parameters.AddWithValue("@CentreNumber", CentreNumber)
                                .Parameters.AddWithValue("@CandidateNumber", CandidateNumber)
                                .Parameters.AddWithValue("@SubjectCode", SubjectCode)
                                '.Parameters.AddWithValue("@SexCode", SexCode)
                                .Parameters.AddWithValue("@TotalMark", TotalMark)
                                'MsgBox("*" & StatusCode & "*")
                                If Attendance = "V" Then
                                    .Parameters.AddWithValue("@Item1", Item1)
                                    .Parameters.AddWithValue("@Item2", Item2)
                                    .Parameters.AddWithValue("@Item3", Item3)
                                    .Parameters.AddWithValue("@Item4", Item4)
                                    .Parameters.AddWithValue("@Item5", Item5)
                                    .Parameters.AddWithValue("@Attendance", 1)
                                ElseIf Attendance = "A" Then
                                    .Parameters.AddWithValue("@Attendance", 0)
                                End If
                                .ExecuteNonQuery()
                            End With
                            sqlcmd.Parameters.Clear()
                            sqlconn.Close()
                        Catch ed As Exception

                            If ed.Message.Contains("Subject") Then
                                Not_registered_subjects.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode)
                            ElseIf ed.Message.Contains("Candidate") Then
                                Not_registered_candidates.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)
                            ElseIf ed.Message.Contains("Centre") Then
                                Not_registered_centres.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode)
                            Else
                                Others_errors.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "-" & ed.Message)
                            End If

                            ReUploadFile.WriteLine(RegionNumber & "," & DistrictNumber & "," & CentreNumber & "," & CandidateNumber & "," & SexCode & "," & SubjectCode & "," & Attendance & "," & TotalMark & "," & Abc & "," & Item)

                            CurrentRecord = ""
                            state.CurrentRecord = "Failed"

                        End Try

                        pct = ((indx + 1) / lineCount) * 100
                        indx = indx + 1

                        'MsgBox(indx & "-" & lineCount & "-" & pct)
                        If pct > 100 Then pct = 100

                        state.pct = pct
                        state.CurrentRecord = CurrentRecord
                        state.CurrentFile = CurentFile
                        state.RecordCount = lineCount
                        worker.ReportProgress(0, state)
                    Loop

                End If


                
                sr.Dispose()
                sr.Dispose()
                CurrentFileNo = CurrentFileNo + 1

                'TextFile.Close()
                Not_registered_subjects.Dispose()
                Not_registered_candidates.Dispose()
                Not_registered_centres.Dispose()
                Others_errors.Dispose()
                ReUploadFile.Dispose()

                Not_registered_candidatesError = My.Computer.FileSystem.GetFileInfo(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_candidates.txt")
                Not_registered_subjectsError = My.Computer.FileSystem.GetFileInfo(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_subjects.txt")
                Not_registered_centresError = My.Computer.FileSystem.GetFileInfo(FilePath & "\" & Replace(filename, ".csv", "") & "-Not_registered_centres.txt")
                Others_errorsError = My.Computer.FileSystem.GetFileInfo(FilePath & "\" & Replace(filename, ".csv", "") & "-Others_errors.txt")
                ReUploadFileError = My.Computer.FileSystem.GetFileInfo(destDirectory & "\" & Replace(filename, ".csv", "") & "-ReUploadFile.csv")

                'MsgBox(fileerror.Length)
                If Not_registered_candidatesError.Length = 0 Then
                    Not_registered_candidatesError.Delete()
                End If
                If Not_registered_subjectsError.Length = 0 Then
                    Not_registered_subjectsError.Delete()
                End If
                If Not_registered_centresError.Length = 0 Then
                    Not_registered_centresError.Delete()
                End If
                If Others_errorsError.Length = 0 Then
                    Others_errorsError.Delete()
                End If
                If ReUploadFileError.Length = 0 Then
                    ReUploadFileError.Delete()
                End If
            Next
        End If
    End Sub

    Private Sub CentreExist(ByVal centreno As String)
        Dim sqlconnz As SqlConnection
        Dim exist As Integer
        sqlconnz = New SqlClient.SqlConnection(p_ConnStr)
        sqlconnz.Open()
        sqlcmd6 = sqlconnz.CreateCommand
        With sqlcmd6
            .CommandType = CommandType.Text
            .CommandTimeout = 0
            .CommandText = "SELECT COUNT(CentreNo) FROM Center WHERE CentreNo = '" & centreno & "'"
            exist = .ExecuteScalar()
        End With
        'sqlcmd6.Parameters.Clear()
        'sqlconnz.Close()
        'sqlconnz.Open()
        If exist = 0 Then
            Using sqlcmd5 As New SqlCommand("dbo.ManipulateCentre")
                sqlcmd5.CommandType = CommandType.StoredProcedure
                sqlcmd5.Parameters.AddWithValue("@CentreNo", centreno)
                sqlcmd5.Parameters.AddWithValue("@CentreName", "UNKNOWN MISSING CENTRE")
                sqlcmd5.Connection = sqlconnz
                sqlcmd5.ExecuteNonQuery()
                sqlconnz.Close()
            End Using
        End If
        sqlconnz.Close()
    End Sub

    Private Sub CopyFailedFile(ByVal filename As String, ByVal sourcepath As String, ByVal targetpath As String)
        ' Dim fileName As String = "test.txt"
        'Dim sourcePath As String = "C:\Users\Public\TestFolder"
        'Dim targetPath As String = "C:\Users\Public\TestFolder\SubDir"
        ' Use Path class to manipulate file and directory paths.
        Dim sourceFile As String = System.IO.Path.Combine(sourcepath, filename)
        Dim destFile As String = System.IO.Path.Combine(targetpath, filename)
        ' To copy a folder's contents to a new location:
        ' Create a new target folder, if necessary.
        If Not System.IO.Directory.Exists(targetpath) Then
            System.IO.Directory.CreateDirectory(targetpath)
        End If
        ' To copy a file to another location and 
        ' overwrite the destination file if it already exists.
        If Not System.IO.File.Exists(destFile) Then
            System.IO.File.Copy(sourceFile, destFile, True)
        End If

    End Sub

    Private Sub newdirectory(ByVal targetpath As String)

        If Not System.IO.Directory.Exists(targetpath) Then
            System.IO.Directory.CreateDirectory(targetpath)
        End If
    End Sub
End Class
