﻿Public Class frmImportMarks

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If FolderBrowserDialog1.ShowDialog() <> DialogResult.OK Then Return
        Me.txtPath.Text = Me.FolderBrowserDialog1.SelectedPath
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoadCSv.Click
        StartThread()
    End Sub

    Private Sub frmImportMarks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub
    Sub StartThread()
        ' This method runs on the main thread. 
        Me.txtCurrentCandidate.Text = ""
        ' Initialize the object that the background worker calls. 
        Dim nm As New UploadMark
        nm.FilePath = Me.txtpath.Text
        ' Start the asynchronous operation.
        BackgroundWorker1.RunWorkerAsync(nm)
        Me.btnLoadCSv.Enabled = False
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim worker As System.ComponentModel.BackgroundWorker
        worker = CType(sender, System.ComponentModel.BackgroundWorker)

        ' Get the Words object and call the main method. 
        Dim nm As UploadMark = CType(e.Argument, UploadMark)
        nm.UploadNewMarks(worker, e)
    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        ' This method runs on the main thread. 
        Dim state As UploadMark.CurrentState =
            CType(e.UserState, UploadMark.CurrentState)
        'MsgBox(state.pct)
        Me.txtCurrentCandidate.Text = state.CurrentRecord.ToString
        Me.ProgressBar1.Minimum = 0
        Me.ProgressBar1.Value = state.pct
        Me.lblpct.Text = CStr(state.pct) & "%"
        'Me.txtrowcount.Text = state.RecordCount

        Me.txtPaper.Text = state.CurrentFile.ToString
        Me.txtpapercount.Text = "File " & state.CurrentFileNo & " of " & state.FileCount
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        If e.Cancelled Then
            Me.lblpct.Text = "0%"
            Me.ProgressBar1.Value = 0
            Me.txtCurrentCandidate.Text = "Failed to Import files. See the Error log file"
            Me.txtCurrentCandidate.ForeColor = Color.Red
            Me.btnLoadCSv.Enabled = True
        Else
            Me.lblpct.Text = "100%"
            Me.txtCurrentCandidate.Text = "Finished Importing all files."
            Me.txtCurrentCandidate.ForeColor = Color.Green
            Me.btnLoadCSv.Enabled = True
        End If
    End Sub
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class