﻿Imports System.Data.SqlClient
Public Class frmRptParameters

    Private Sub frmRptParameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Combo("tbl_regions", "szRegionNumber", "szRegionName", Me.cmbRegion)
        combowhere("View_region_district", "szDistrictNumber", "szDistrictName", "szRegionNumber", cmbRegion.SelectedValue, Me.cmbDistrict)
        combowhere1("View_centres", "szExamCentreNumber", "szExamCentreName", "szRegionNumber", "szDistrictNumber", cmbRegion.SelectedValue, cmbDistrict.SelectedValue, Me.cmbCentre)
    End Sub


    Private Sub rdRegion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdRegion.CheckedChanged
        If Me.rdRegion.Checked = True Then
            Me.cmbDistrict.Enabled = False
            Me.cmbCentre.Enabled = False
            Me.cmbRegion.Enabled = True
            Me.Gpcandidate.Enabled = False
        End If
    End Sub

    Private Sub rdDistrict_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdDistrict.CheckedChanged
        If Me.rdDistrict.Checked = True Then
            Me.cmbRegion.Enabled = True
            Me.cmbCentre.Enabled = False
            Me.cmbDistrict.Enabled = True
            Me.Gpcandidate.Enabled = False
        End If
    End Sub

    Private Sub rdCentre_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdCentre.CheckedChanged
        If Me.rdCentre.Checked = True Then
            Me.cmbDistrict.Enabled = True
            Me.cmbCentre.Enabled = True
            Me.cmbRegion.Enabled = True
            Me.Gpcandidate.Enabled = False
        End If
    End Sub


    Private Sub cmbRegion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRegion.SelectedIndexChanged
        On Error Resume Next
        combowhere("View_region_district", "szDistrictNumber", "szDistrictName", "szRegionNumber", cmbRegion.SelectedValue, Me.cmbDistrict)
        combowhere1("View_centres", "szExamCentreNumber", "szExamCentreName", "szRegionNumber", "szDistrictNumber", cmbRegion.SelectedValue, cmbDistrict.SelectedValue, Me.cmbCentre)
    End Sub

    Private Sub cmbDistrict_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDistrict.SelectedIndexChanged
        On Error Resume Next
        combowhere1("View_centres", "szExamCentreNumber", "szExamCentreName", "szRegionNumber", "szDistrictNumber", cmbRegion.SelectedValue, cmbDistrict.SelectedValue, Me.cmbCentre)
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        pnerbatch = 0
        Dim val As String = ""
        If Me.rdAll.Checked = True Then
            pReportLevel = "All"

        ElseIf Me.rdRegion.Checked = True Then
            pRegionNumber = Me.cmbRegion.SelectedValue
            pReportLevel = "Region"
            val = pRegionNumber
        ElseIf Me.rdDistrict.Checked = True Then
            pRegionNumber = Me.cmbRegion.SelectedValue
            pDidtrictNumber = Me.cmbDistrict.SelectedValue
            pReportLevel = "District"
            val = pRegionNumber & "-" & pDidtrictNumber
        ElseIf Me.rdCentre.Checked = True Then
            pRegionNumber = Me.cmbRegion.SelectedValue
            pDidtrictNumber = Me.cmbDistrict.SelectedValue
            pCentreNumber = Me.cmbCentre.SelectedValue
            pReportLevel = "Centre"
            val = pCentreNumber
        ElseIf Me.rdcandrange.Checked = True Then
            pCandidateFrom = Me.txtCandidateFrom.Text
            pCandidateTo = Me.txtCandidateTo.Text
            pReportLevel = "Candidate"
            val = pCentreNumber
        ElseIf Me.rdRegionCand.Checked = True Then
            pRegionNumber = Me.cmbRegion.SelectedValue
            pCandidateFrom = Me.txtCandidateFrom.Text
            pCandidateTo = Me.txtCandidateTo.Text
            pReportLevel = "RegionCandidate"
            val = pCentreNumber
        End If

        Dim frm As New frmRpts
        frm.Show()
        ' Me.Hide()
    End Sub

    Private Sub rdAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdAll.CheckedChanged
        If Me.rdAll.Checked = True Then
            Me.cmbRegion.Enabled = False
            Me.cmbDistrict.Enabled = False
            Me.cmbCentre.Enabled = False
            Me.Gpcandidate.Enabled = False
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub rdcandrange_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdcandrange.CheckedChanged
        If Me.rdcandrange.Checked = True Then
            Me.cmbRegion.Enabled = False
            Me.cmbDistrict.Enabled = False
            Me.cmbCentre.Enabled = False
            Me.Gpcandidate.Enabled = True
        End If
    End Sub

    Private Sub btnBatch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBatch.Click
        If rdRegion.Checked = True And Me.cmbRegion.Text <> "" Then
            pnerbatch = 1
            pRegionNumber = Me.cmbRegion.SelectedValue
            Dim frm As New frmRpts
            frm.Show()
            'Me.Close()
        Else
            MsgBox("You must select Region option and Region Name!")
        End If
    End Sub

    Private Sub rdRegionCand_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdRegionCand.CheckedChanged
        If Me.rdRegionCand.Checked = True Then
            Me.cmbRegion.Enabled = True
            Me.cmbDistrict.Enabled = False
            Me.cmbCentre.Enabled = False
            Me.Gpcandidate.Enabled = True
        End If
    End Sub


    Private Sub btnListGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListGenerate.Click
        Dim isExport As String = "-1"

        isExport = CheckExportError(Me.cmbRegion.SelectedValue)
        'isExport = "0"

        If isExport = "0" Then


            Me.sfdText.ShowDialog()


            If Trim(sfdText.FileName) = "" Then
                MsgBox("PLEASE WRITE NAME OF CSV FILE YOU WANT TO CREATE")
            Else
                Me.Cursor = Cursors.WaitCursor
                ptextfilename = sfdText.FileName




                Dim val As String = ""
                If Me.rdAll.Checked = True Then
                    pReportLevel = "All"

                ElseIf Me.rdRegion.Checked = True Then
                    pRegionNumber = Me.cmbRegion.SelectedValue
                    pReportLevel = "Region"
                    val = pRegionNumber
                ElseIf Me.rdDistrict.Checked = True Then
                    pRegionNumber = Me.cmbRegion.SelectedValue
                    pDidtrictNumber = Me.cmbDistrict.SelectedValue
                    pReportLevel = "District"
                    val = pRegionNumber & "-" & pDidtrictNumber
                ElseIf Me.rdCentre.Checked = True Then
                    pRegionNumber = Me.cmbRegion.SelectedValue
                    pDidtrictNumber = Me.cmbDistrict.SelectedValue
                    pCentreNumber = Me.cmbCentre.SelectedValue
                    pReportLevel = "Centre"
                    val = pCentreNumber
                ElseIf Me.rdcandrange.Checked = True Then
                    pCandidateFrom = Me.txtCandidateFrom.Text
                    pCandidateTo = Me.txtCandidateTo.Text
                    pReportLevel = "Candidate"
                    val = pCentreNumber
                End If
                ' Me.Hide()
                Dim frm As New frmRpts
                frm.Show()
                Me.Hide()
            End If

        ElseIf isExport = "1" Then
            MsgBox("Not ready for export CSV.There are list of candidates from OMR but not in Kituoni side and Vise versa. Also Attendance differed", MsgBoxStyle.Critical)


        ElseIf isExport = "2" Then
            MsgBox("Not ready for export CSV.There are list of candidates from OMR but not in Kituoni side and Vise versa", MsgBoxStyle.Critical)

        ElseIf isExport = "3" Then
            MsgBox("Not ready for export CSV.There are list of candidates from OMR but not in Kituoni side and Attendance differed", MsgBoxStyle.Critical)

        ElseIf isExport = "4" Then
            MsgBox("Not ready for export CSV.There are list of candidates from Kituoni but not in OMR side and Attendance differed", MsgBoxStyle.Critical)

        ElseIf isExport = "5" Then
            MsgBox("Not ready for export CSV.There are list of candidates from OMR but not in Kituoni side", MsgBoxStyle.Critical)

        ElseIf isExport = "6" Then
            MsgBox("Not ready for export CSV.There are list of candidates from Kituoni but not in OMR side", MsgBoxStyle.Critical)
        ElseIf isExport = "7" Then
            MsgBox("Not ready for export CSV. Attendance differed", MsgBoxStyle.Critical)

        End If











    End Sub

    Private Function CheckExportError(ByVal RegNo As String) As String
        Dim sqlconn2 As New SqlConnection
        Dim drh2 As SqlDataReader
        Dim sqlconn1 As New SqlConnection
        Dim drh1 As SqlDataReader
        Dim sqlconn3 As New SqlConnection
        Dim drh3 As SqlDataReader
        Dim GetRegnoOMR As String = "-1"
        Dim GetRegnoKituoni As String = "-1"
        Dim GetRegnoAttDiffer As String = "-1"
        Dim ExportList As String = "-1"


        Try

            sqlconn1 = New SqlClient.SqlConnection(p_ConnStr)

            sqlconn1.Open()
            sqlcmd1 = sqlconn1.CreateCommand
            With sqlcmd1
                .CommandType = CommandType.Text
                .CommandText = "select top 1 substring(c.RegionNumber,3,2) Regno" & _
                                " from cs_ListOMR c" & _
                                " Left join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber" & _
                                " and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode" & _
                                " where a.RegionNumber is null and a.DistrictNumber is null and a.CentreNumber is null and a.CandidateNumber is null" & _
                                " and substring(c.RegionNumber,3,2)='" & RegNo & "'"

                drh1 = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh1.Read()
                GetRegnoOMR = drh1(0).ToString
            End While
            sqlconn1.Close()

        Catch err As Exception
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "OMR Only")
            sqlconn1.Close()
        End Try


        Try

            sqlconn2 = New SqlClient.SqlConnection(p_ConnStr)

            sqlconn2.Open()
            sqlcmd2 = sqlconn2.CreateCommand
            With sqlcmd2
                .CommandType = CommandType.Text
                .CommandText = "select top 1 substring(a.RegionNumber,3,2) Regno" & _
                                " from cs_ListOMR c" & _
                                " right join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber" & _
                                " and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode" & _
                                " where c.RegionNumber is null and c.DistrictNumber is null and c.CentreNumber is null and c.CandidateNumber is null" & _
                                " and substring(a.RegionNumber,3,2)='" & RegNo & "'"

                drh2 = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh2.Read()
                GetRegnoKituoni = drh2(0).ToString
            End While
            sqlconn2.Close()

        Catch err As Exception
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Kituoni Only")
            sqlconn2.Close()
        End Try

        Try

            sqlconn3 = New SqlClient.SqlConnection(p_ConnStr)

            sqlconn3.Open()
            sqlcmd3 = sqlconn3.CreateCommand
            With sqlcmd3
                .CommandType = CommandType.Text
                .CommandText = "select top 1 substring(c.RegionNumber,3,2) Regno" & _
                                " from cs_ListOMR c" & _
                                " inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber" & _
                                " and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode" & _
                                " where c.Attendance <> a.Attendance" & _
                                " and substring(c.RegionNumber,3,2)='" & RegNo & "'"

                drh3 = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh3.Read()
                GetRegnoAttDiffer = drh3(0).ToString
            End While
            sqlconn3.Close()

        Catch err As Exception
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "AttDiffer Only")
            sqlconn2.Close()
        End Try



        If GetRegnoKituoni = RegNo And GetRegnoOMR = RegNo And GetRegnoAttDiffer = RegNo Then

            ExportList = "1" 'Not ready for export CSV.There are list of candidates from Kituoni but not in OMR side 

        ElseIf GetRegnoOMR = RegNo And GetRegnoKituoni = RegNo Then

            ExportList = "2" 'Not ready for export CSV.There are list of candidatess from OMR but not in Kituoni side 

        ElseIf GetRegnoOMR = RegNo And GetRegnoAttDiffer = RegNo Then

            ExportList = "3" 'Not ready for export CSV.There are list of candidatess from OMR but not in Kituoni side 

        ElseIf GetRegnoKituoni = RegNo And GetRegnoAttDiffer = RegNo Then

            ExportList = "4" 'Not ready for export CSV.There are list of candidatess from OMR but not in Kituoni side 

        ElseIf GetRegnoOMR = RegNo Then

            ExportList = "5" 'Not ready for export CSV.There are list of candidatess differed attendance status 

        ElseIf GetRegnoKituoni = RegNo Then

            ExportList = "6" 'Not ready for export CSV.There are list of candidatess differed attendance status 

        ElseIf GetRegnoAttDiffer = RegNo Then

            ExportList = "7" 'Not ready for export CSV.There are list of candidatess differed attendance status 

        Else
            ExportList = "0" 'Ready for export CSV
        End If
        Return ExportList
    End Function


End Class