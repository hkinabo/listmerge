﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportMarks
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.txtCurrentCandidate = New System.Windows.Forms.TextBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.lblpct = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.btnLoadCSv = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtpapercount = New System.Windows.Forms.TextBox()
        Me.txtPaper = New System.Windows.Forms.TextBox()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(539, 201)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(124, 36)
        Me.btnClose.TabIndex = 80
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtCurrentCandidate
        '
        Me.txtCurrentCandidate.Enabled = False
        Me.txtCurrentCandidate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrentCandidate.Location = New System.Drawing.Point(23, 156)
        Me.txtCurrentCandidate.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtCurrentCandidate.Name = "txtCurrentCandidate"
        Me.txtCurrentCandidate.Size = New System.Drawing.Size(639, 30)
        Me.txtCurrentCandidate.TabIndex = 79
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'lblpct
        '
        Me.lblpct.AutoSize = True
        Me.lblpct.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblpct.Location = New System.Drawing.Point(629, 127)
        Me.lblpct.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblpct.Name = "lblpct"
        Me.lblpct.Size = New System.Drawing.Size(0, 20)
        Me.lblpct.TabIndex = 78
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(23, 122)
        Me.ProgressBar1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(601, 28)
        Me.ProgressBar1.TabIndex = 77
        '
        'btnLoadCSv
        '
        Me.btnLoadCSv.Location = New System.Drawing.Point(357, 199)
        Me.btnLoadCSv.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnLoadCSv.Name = "btnLoadCSv"
        Me.btnLoadCSv.Size = New System.Drawing.Size(173, 37)
        Me.btnLoadCSv.TabIndex = 76
        Me.btnLoadCSv.Text = "Upload Marks"
        Me.btnLoadCSv.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(541, 33)
        Me.btnBrowse.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(121, 34)
        Me.btnBrowse.TabIndex = 72
        Me.btnBrowse.Text = "Browse Folder"
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'txtpapercount
        '
        Me.txtpapercount.Enabled = False
        Me.txtpapercount.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpapercount.Location = New System.Drawing.Point(23, 69)
        Me.txtpapercount.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtpapercount.Name = "txtpapercount"
        Me.txtpapercount.Size = New System.Drawing.Size(211, 29)
        Me.txtpapercount.TabIndex = 71
        '
        'txtPaper
        '
        Me.txtPaper.Enabled = False
        Me.txtPaper.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaper.Location = New System.Drawing.Point(235, 69)
        Me.txtPaper.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPaper.Name = "txtPaper"
        Me.txtPaper.Size = New System.Drawing.Size(427, 29)
        Me.txtPaper.TabIndex = 70
        '
        'txtPath
        '
        Me.txtPath.Enabled = False
        Me.txtPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPath.Location = New System.Drawing.Point(23, 34)
        Me.txtPath.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(517, 30)
        Me.txtPath.TabIndex = 69
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.btnClose)
        Me.GroupBox1.Controls.Add(Me.txtPaper)
        Me.GroupBox1.Controls.Add(Me.txtCurrentCandidate)
        Me.GroupBox1.Controls.Add(Me.txtpapercount)
        Me.GroupBox1.Controls.Add(Me.lblpct)
        Me.GroupBox1.Controls.Add(Me.btnBrowse)
        Me.GroupBox1.Controls.Add(Me.ProgressBar1)
        Me.GroupBox1.Controls.Add(Me.btnLoadCSv)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Location = New System.Drawing.Point(15, 15)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(676, 254)
        Me.GroupBox1.TabIndex = 81
        Me.GroupBox1.TabStop = False
        '
        'frmImportMarks
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(707, 278)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmImportMarks"
        Me.Text = "IMPORT CANDIDATE PAPER MARKS"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtCurrentCandidate As System.Windows.Forms.TextBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblpct As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents btnLoadCSv As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents txtpapercount As System.Windows.Forms.TextBox
    Friend WithEvents txtPaper As System.Windows.Forms.TextBox
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
