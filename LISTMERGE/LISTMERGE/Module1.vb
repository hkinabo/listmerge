﻿Imports System.Data.SqlClient
Module Module1
    Public sqlcmd0 As SqlCommand
    Public sqlcmd As SqlCommand
    Public sqlcmd1 As SqlCommand
    Public sqlcmd2 As SqlCommand
    Public sqlcmd3 As SqlCommand
    Public sqlcmd4 As SqlCommand
    Public sqlcmd5 As SqlCommand
    Public sqlcmd6 As SqlCommand

    Public sqlconn As SqlConnection
    Public p_ConnStr As String '= "Data Source=ICTPC3;Initial Catalog= PSLE2016 ;User ID=sa;password =2009necta;Connection Timeout=0"
    Public p_ConnStr1 As String
    Public p_username As String
    Public p_password As String
    Public p_dbname As String
    Public p_servername As String
    Public p_dbserver As String
    Public pExamType As Integer
    Public pExamName As String
    Public logindialogflag As Integer
    Public rptname As String
    Public cNo As String
    Public zNumber As String
    Public caNo As String
    Public cNam As String
    Public pFname As String
    Public pMiddle As String
    Public pSname As String
    Public qualifyStatus As Integer

    Public pRegionNumber As String
    Public pDidtrictNumber As String
    Public pCentreNumber As String
    Public pCandidateFrom As String
    Public pCandidateTo As String
    Public pReportLevel As String
    Public pReportType As String
    Public pVisionType As String
    Public pUrl As String
    Public pnerbatch As Integer
    Public ptextfilename As String

    Public querryid As Integer
    Public exam_centre_id As String
    Public exam_type_id As Integer
    Public regionid As String
    Public districtid As String
    Public candidatenumber As String
    Public candidatetype As String

    Public pCriteria As Integer
    Public pPrivateSchool As Integer
    Public pPrintDirect As Integer
    Public pPrintBy As Integer
    Public pUplodBy As Integer

    Public P_add As Integer
    Public p_GeneralParamCaller As String

    Public pStartDate As Date
    Public pEndDate As Date

    Public pLessThan As Integer
    Public pWithheldStatus As String


    Public Sub desc(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldvalue As String, ByVal fieldTextbox As TextBox)
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "select " & fieldcode & "," & fielddesc & " from " & tablename & " WHERE " & fieldwhere & " = '" & fieldvalue & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                fieldTextbox.Text = ""
                fieldTextbox.Text = drh(1)
            End While
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub desc2(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldwhere1 As String, ByVal fieldvalue As String, ByVal fieldvalue1 As String, ByVal fieldTextbox As TextBox)
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "select " & fieldcode & "," & fielddesc & " from " & tablename & " WHERE " & fieldwhere & " = '" & fieldvalue & "' AND " & fieldwhere1 & " = '" & fieldvalue1 & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                fieldTextbox.Text = ""
                fieldTextbox.Text = drh(1)
            End While
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub desc3(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldwhere1 As String, ByVal fieldwhere2 As String, ByVal fieldvalue As String, ByVal fieldvalue1 As String, ByVal fieldvalue2 As String, ByVal fieldTextbox As TextBox)
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "select " & fieldcode & "," & fielddesc & " from " & tablename & " WHERE " & fieldwhere & " = '" & fieldvalue & "' AND " & fieldwhere1 & " = '" & fieldvalue1 & "' AND " & fieldwhere2 & " = '" & fieldvalue2 & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                fieldTextbox.Text = ""
                fieldTextbox.Text = drh(1)
            End While
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub Combo(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldCombo As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("EXEC PopulateComboValues " & tablename & "," & fieldcode & "," & fielddesc, sqlconn)
            da.Fill(data_table)
            fieldCombo.DataSource = data_table
            fieldCombo.DisplayMember = fielddesc
            fieldCombo.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub ComboEMISDB(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldCombo As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr1)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("EXEC PopulateComboValues " & tablename & "," & fieldcode & "," & fielddesc, sqlconn)
            da.Fill(data_table)
            fieldCombo.DataSource = data_table
            fieldCombo.DisplayMember = fielddesc
            fieldCombo.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub combowhere(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldvalue As String, ByVal fieldTextbox As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("EXEC PopulateComboValues " & tablename & "," & fieldcode & "," & fielddesc & "," & fieldwhere & "," & "NULL" & "," & "NULL" & "," & fieldvalue & "," & "NULL" & "," & "NULL", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            'MsgBox(err.Message)
        End Try
    End Sub
    Public Sub combowhereEMISDB(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldvalue As String, ByVal fieldTextbox As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr1)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("EXEC PopulateComboValues " & tablename & "," & fieldcode & "," & fielddesc & "," & fieldwhere & "," & "NULL" & "," & "NULL" & "," & fieldvalue & "," & "NULL" & "," & "NULL", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            'MsgBox(err.Message)
        End Try
    End Sub
    Public Sub combowhereEMISDB1(ByVal tablename As String, ByVal tablename2 As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere1 As String, ByVal fieldwhere2 As String, ByVal fieldvalue1 As String, ByVal fieldTextbox As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("SELECT " & fieldcode & "," & fielddesc & " " & fielddesc & " FROM " & tablename & " WHERE " & fieldwhere1 & " IN ( " & "SELECT " & fieldcode & " FROM " & tablename2 & " WHERE " & fieldwhere2 & " = '" & fieldvalue1 & "'" & " ) ", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            'MsgBox(err.Message)
        End Try
    End Sub
    Public Sub combowhere1(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere1 As String, ByVal fieldwhere2 As String, ByVal fieldvalue1 As String, ByVal fieldvalue2 As String, ByVal fieldTextbox As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("EXEC PopulateComboValues " & tablename & "," & fieldcode & "," & fielddesc & "," & fieldwhere1 & "," & fieldwhere2 & "," & "NULL" & "," & fieldvalue1 & "," & fieldvalue2 & "," & "NULL", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub listload(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal listbox1 As ListView)
        Try
            Dim drh1 As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "select " & fieldcode & "," & fielddesc & " from " & tablename
                drh1 = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            Dim k As Integer
            k = 0
            While drh1.Read()
                listbox1.Items.Add(drh1(0))
                k = k + 1
            End While
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub listformloadwhere(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldvalue As String, ByVal fielddatagrid As DataGridView)
        Try
            Dim drh1 As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "select " & fieldcode & "," & fielddesc & " from " & tablename & " WHERE " & fieldwhere & " = '" & fieldvalue & "'"
                drh1 = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            Dim k As Integer
            k = 0
            While drh1.Read()
                fielddatagrid.Rows.Add()
                fielddatagrid.Item(0, k).Value = drh1(0)
                fielddatagrid.Item(1, k).Value = drh1(1)
                k = k + 1
            End While
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub comboSimple(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldTextbox As ComboBox)
        Try

            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("SELECT " & fieldcode & "," & fielddesc & " " & fielddesc & " FROM " & tablename & " ORDER BY 1 ", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Sub combowhereIN(ByVal tablename As String, ByVal fieldcode As String, ByVal fielddesc As String, ByVal fieldwhere As String, ByVal fieldvalue As String, ByVal fieldTextbox As ComboBox)
        Try
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            Dim data_table As New DataTable(tablename)
            Dim da As New SqlDataAdapter("SELECT " & fieldcode & "," & fielddesc & " " & fielddesc & " FROM " & tablename & " WHERE " & fieldwhere & " IN (" & fieldvalue & ")", sqlconn)
            da.Fill(data_table)
            fieldTextbox.DataSource = data_table
            fieldTextbox.DisplayMember = fielddesc
            fieldTextbox.ValueMember = fieldcode
            sqlconn.Close()
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message)
        End Try
    End Sub
    Public Function GetRegionNo(ByVal RegionName As String) As String
        Dim rNo As String = ""
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "SELECT RegionNumber FROM [Region] where RegionName = '" & Trim(RegionName) & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)

            End With


            While drh.Read()

                rNo = drh("RegionNumber").ToString

            End While

        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "FTNA")
        End Try
        sqlconn.Close()
        Return rNo
    End Function

    Public Sub HighlightItemListBox(ByVal searchString As String, ByVal listBoxName As ListBox)
        ' Set the SelectionMode property of the ListBox to select multiple items.
        'lstRegion.SelectionMode = SelectionMode.MultiExtended

        ' Set our intial index variable to -1. 
        Dim x As Integer = -1
        ' If the search string is empty exit. 
        If searchString.Length <> 0 Then
            ' Loop through and find each item that matches the search string. 
            Do
                ' Retrieve the item based on the previous index found. Starts with -1 which searches start.
                x = listBoxName.FindString(searchString, x)
                ' If no item is found that matches exit. 
                If x <> -1 Then
                    ' Since the FindString loops infinitely, determine if we found first item again and exit. 
                    If listBoxName.SelectedIndices.Count > 0 Then
                        If x = listBoxName.SelectedIndices(0) Then
                            Return
                        End If
                    End If
                    ' Select the item in the ListBox once it is found.
                    listBoxName.SetSelected(x, True)
                End If
            Loop While x <> -1
        End If
    End Sub

    Public Function CheckUserCentre(ByVal centNo As String) As String
        Dim MyCentre As String = "-1"
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "SELECT isnull(b.CentreNo,-1) FROM UserCentre b" & _
                    " where  b.CentreNo = '" & centNo & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                MyCentre = (drh(0).ToString)
            End While
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "FTNA")
        End Try
        sqlconn.Close()
        Return MyCentre
    End Function

    Public Function getUserNameCentre(ByVal centNo As String) As String
        getUserNameCentre = ""
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "SELECT b.LoginName FROM UserCentre b" & _
                    " where  b.CentreNo = '" & centNo & "'"
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                getUserNameCentre = (drh(0).ToString)
            End While
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "FTNA")
        End Try
        sqlconn.Close()
        Return getUserNameCentre
    End Function

    Public Function CheckUserGroup(ByVal GroupName As String, ByVal loginName As String) As String
        Dim MyGroup As String = "-1"
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "SELECT isnull(b.GroupName,-1) FROM UserGroup b" & _
                    " where  b.GroupName = '" & GroupName & "' and b.LoginName = '" & loginName & "' "
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                MyGroup = (drh(0).ToString)
            End While
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "FTNA")
        End Try
        sqlconn.Close()
        Return MyGroup
    End Function

    Public Function GetParameter(ByVal hdr As Integer, ByVal dtl As Integer) As String
        Dim param As String = ""
        Try
            Dim drh As SqlDataReader
            sqlconn = New SqlClient.SqlConnection(p_ConnStr)
            sqlconn.Open()
            sqlcmd = sqlconn.CreateCommand
            With sqlcmd
                .CommandType = CommandType.Text
                .CommandText = "SELECT DBO.GENERALGETPARAMETER(" & hdr & "," & dtl & ")" 
                drh = .ExecuteReader(CommandBehavior.CloseConnection)
            End With
            While drh.Read()
                param = (drh(0).ToString)
            End While
        Catch err As Exception
            sqlconn.Close()
            MsgBox(err.Message, MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "FTNA")
        End Try
        sqlconn.Close()
        Return param
    End Function
End Module

