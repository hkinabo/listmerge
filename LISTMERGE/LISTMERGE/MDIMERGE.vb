﻿Imports System.Windows.Forms

Public Class MDIMERGE

    Private Sub ShowNewForm(ByVal sender As Object, ByVal e As EventArgs)
        ' Create a new instance of the child form.
        Dim ChildForm As New System.Windows.Forms.Form
        ' Make it a child of this MDI form before showing it.
        ChildForm.MdiParent = Me

        m_ChildFormNumber += 1
        ChildForm.Text = "Window " & m_ChildFormNumber

        ChildForm.Show()
    End Sub

    Private Sub OpenFile(ByVal sender As Object, ByVal e As EventArgs)
        Dim OpenFileDialog As New OpenFileDialog
        OpenFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        OpenFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"
        If (OpenFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = OpenFileDialog.FileName
            ' TODO: Add code here to open the file.
        End If
    End Sub

    Private Sub SaveAsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim SaveFileDialog As New SaveFileDialog
        SaveFileDialog.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        SaveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*"

        If (SaveFileDialog.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
            Dim FileName As String = SaveFileDialog.FileName
            ' TODO: Add code here to save the current contents of the form to a file.
        End If
    End Sub


    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Close()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Use My.Computer.Clipboard to insert the selected text or images into the clipboard
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Use My.Computer.Clipboard.GetText() or My.Computer.Clipboard.GetData to retrieve information from the clipboard.
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub

    Private Sub CloseAllToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs)
        ' Close all child forms of the parent.
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
    End Sub

    Private m_ChildFormNumber As Integer

    Private Sub EXITToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EXITToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub IMPORTToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IMPORTToolStripMenuItem.Click
        pUplodBy = 1
        Dim frm As New frmImportMarks
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.btnLoadCSv.Text = "Upload OMR List1"
        frm.Show()
    End Sub

    Private Sub IMPORTToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IMPORTToolStripMenuItem1.Click
        pUplodBy = 2
        Dim frm As New frmImportMarks
        frm.MdiParent = Me
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.btnLoadCSv.Text = "Upload Kituoni List1"
        frm.Show()
    End Sub

    Private Sub PREVIEWToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PREVIEWToolStripMenuItem.Click
        rptname = "OMRPreview"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.rdAll.Enabled = False
        frm.rdcandrange.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub MDIMERGE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmLoginForm.ShowDialog()
    End Sub

    Private Sub LIST1ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LIST1ToolStripMenuItem1.Click
      

        rptname = "ListGenerateOne"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.btnListGenerate.Visible = True
        frm.btnListGenerate.Text = "CSV List 1"
        frm.btnPreview.Visible = False
        frm.rdAll.Enabled = False
        frm.rdcandrange.Enabled = False
        frm.rdCentre.Enabled = False
        frm.rdDistrict.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub LIST2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LIST2ToolStripMenuItem.Click
        rptname = "ListGenerateTwo"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.btnListGenerate.Visible = True
        frm.btnListGenerate.Text = "CSV List 2"
        frm.btnPreview.Visible = False
        frm.rdAll.Enabled = False
        frm.rdcandrange.Enabled = False
        frm.rdCentre.Enabled = False
        frm.rdDistrict.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub OMRONLYToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OMRONLYToolStripMenuItem.Click
        rptname = "OMROnly"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.rdcandrange.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub KITUONIONLYToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KITUONIONLYToolStripMenuItem.Click
        rptname = "KituoniOnly"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.rdcandrange.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub ATTENDANCEDIFFERToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ATTENDANCEDIFFERToolStripMenuItem.Click
        rptname = "AttendanceDiffer"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.rdcandrange.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub

    Private Sub PREVIEWToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PREVIEWToolStripMenuItem1.Click
        rptname = "KituoniPreview"
        Dim frm As New frmRptParameters
        frm.MdiParent = Me
        frm.rdAll.Enabled = False
        frm.rdcandrange.Enabled = False
        frm.rdRegionCand.Enabled = False
        frm.StartPosition = FormStartPosition.CenterScreen
        frm.Show()
    End Sub
End Class
