Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.ReportSource

Public Class frmRpts
    Private Sub frmRpts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
     
        If rptname = "ListGenerateOne" Then
            Dim batchname As String

            Try

                batchname = ptextfilename + ".csv"



                Dim myReport As New rptListGenerateOne

                LogonReport(myReport)
                myReport.SetParameterValue("@RegionNumber", pRegionNumber)
                myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
                myReport.SetParameterValue("@CentreNo", pCentreNumber)
                myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
                myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
                myReport.SetParameterValue("@ReportLevel", pReportLevel)

                Me.CRV.ReportSource = myReport

                Dim crexpopt As ExportOptions
                Dim disksave As New DiskFileDestinationOptions
                Dim crformattype As New CharacterSeparatedValuesFormatOptions



                crformattype.Delimiter = ""

                crformattype.SeparatorText = ","

                disksave.DiskFileName = batchname

                crexpopt = myReport.ExportOptions

                With crexpopt
                    .ExportDestinationType = ExportDestinationType.DiskFile
                    .ExportFormatType = ExportFormatType.CharacterSeparatedValues

                    .ExportDestinationOptions = disksave
                    .ExportFormatOptions = crformattype
                End With

                myReport.Export(crexpopt)

                'myReport.Close()
                'myReport.Dispose()


            Catch ed As Exception
                MsgBox(ed.ToString)
            End Try
            sqlcmd.Parameters.Clear()
            sqlconn.Close()
            ' CRV.AllowedExportFormats = 7
            MessageBox.Show("CSV List One has been saved", "Save", MessageBoxButtons.OK)
            Me.Close()
        ElseIf rptname = "ListGenerateTwo" Then
            Dim batchname As String

            Try

                batchname = ptextfilename + ".csv"



                Dim myReport As New rptrptListGenerateTwo

                LogonReport(myReport)
                myReport.SetParameterValue("@RegionNumber", pRegionNumber)
                myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
                myReport.SetParameterValue("@CentreNo", pCentreNumber)
                myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
                myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
                myReport.SetParameterValue("@ReportLevel", pReportLevel)

                Me.CRV.ReportSource = myReport

                Dim crexpopt As ExportOptions
                Dim disksave As New DiskFileDestinationOptions
                Dim crformattype As New CharacterSeparatedValuesFormatOptions

                crformattype.Delimiter = ""

                crformattype.SeparatorText = ","

                disksave.DiskFileName = batchname

                crexpopt = myReport.ExportOptions

                With crexpopt
                    .ExportDestinationType = ExportDestinationType.DiskFile
                    .ExportFormatType = ExportFormatType.CharacterSeparatedValues
                    .ExportDestinationOptions = disksave
                    .ExportFormatOptions = crformattype
                End With

                myReport.Export(crexpopt)

                ' myReport.Close()
               ' myReport.Dispose()
                ' End While

            Catch ed As Exception
                MsgBox(ed.ToString)
            End Try
            sqlcmd.Parameters.Clear()
            sqlconn.Close()
            ' CRV.AllowedExportFormats = 7
            MessageBox.Show("CSV List Two has been saved", "Save", MessageBoxButtons.OK)
            Me.Close()
        ElseIf rptname = "OMROnly" Then
            Dim myReport As New rptListOnlyOMR
            LogonReport(myReport)
            myReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            Me.CRV.Zoom(1)
            Me.CRV.ShowPrintButton = True
            Me.CRV.ShowExportButton = True
            myReport.SetParameterValue("@RegionNumber", pRegionNumber)
            myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
            myReport.SetParameterValue("@CentreNo", pCentreNumber)
            myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
            myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
            myReport.SetParameterValue("@ReportLevel", pReportLevel)
            Me.CRV.ReportSource = myReport
            Me.WindowState = FormWindowState.Maximized
        ElseIf rptname = "KituoniOnly" Then
            Dim myReport As New rptListOnlyKituoni
            LogonReport(myReport)
            myReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            Me.CRV.Zoom(1)
            Me.CRV.ShowPrintButton = True
            Me.CRV.ShowExportButton = True
            myReport.SetParameterValue("@RegionNumber", pRegionNumber)
            myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
            myReport.SetParameterValue("@CentreNo", pCentreNumber)
            myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
            myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
            myReport.SetParameterValue("@ReportLevel", pReportLevel)
            Me.CRV.ReportSource = myReport
            Me.WindowState = FormWindowState.Maximized
        ElseIf rptname = "AttendanceDiffer" Then
            Dim myReport As New rptListAttendanceDiffer
            LogonReport(myReport)
            myReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            Me.CRV.Zoom(1)
            Me.CRV.ShowPrintButton = True
            Me.CRV.ShowExportButton = True
            myReport.SetParameterValue("@RegionNumber", pRegionNumber)
            myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
            myReport.SetParameterValue("@CentreNo", pCentreNumber)
            myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
            myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
            myReport.SetParameterValue("@ReportLevel", pReportLevel)
            Me.CRV.ReportSource = myReport
            Me.WindowState = FormWindowState.Maximized
        ElseIf rptname = "OMRPreview" Then
            Dim myReport As New rptListOMRPreview
            LogonReport(myReport)
            myReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            Me.CRV.Zoom(1)
            Me.CRV.ShowPrintButton = True
            Me.CRV.ShowExportButton = True
            myReport.SetParameterValue("@RegionNumber", pRegionNumber)
            myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
            myReport.SetParameterValue("@CentreNo", pCentreNumber)
            myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
            myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
            myReport.SetParameterValue("@ReportLevel", pReportLevel)
            Me.CRV.ReportSource = myReport
            Me.WindowState = FormWindowState.Maximized
        ElseIf rptname = "KituoniPreview" Then
            Dim myReport As New rptListKituoniPreview
            LogonReport(myReport)
            myReport.PrintOptions.PaperSize = CrystalDecisions.Shared.PaperSize.PaperA4
            Me.CRV.Zoom(1)
            Me.CRV.ShowPrintButton = True
            Me.CRV.ShowExportButton = True
            myReport.SetParameterValue("@RegionNumber", pRegionNumber)
            myReport.SetParameterValue("@DistrictNo", pDidtrictNumber)
            myReport.SetParameterValue("@CentreNo", pCentreNumber)
            myReport.SetParameterValue("@Candidate_No_From", pCandidateFrom)
            myReport.SetParameterValue("@Candidate_No_To", pCandidateTo)
            myReport.SetParameterValue("@ReportLevel", pReportLevel)
            Me.CRV.ReportSource = myReport
            Me.WindowState = FormWindowState.Maximized
        End If

        ' Me.WindowState = FormWindowState.Maximized
    End Sub

    Public Sub LogonReport(ByVal rptReport As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim crReportDocument As New ReportDocument()
        Dim crTableLogonInfos As New TableLogOnInfos()
        Dim crTableLogonInfo As New TableLogOnInfo()
        Dim crTables As Tables
        Dim crTable As Table
        Dim crSections As Sections
        Dim crSection As Section
        Dim crReportObjects As ReportObjects
        Dim crReportObject As ReportObject
        Dim crSubreportObject As SubreportObject
        Dim crDataBase As Database
        Dim crConnInfo As New ConnectionInfo()
        Dim subRepDoc As New ReportDocument()
        Try
            'Logs into the tables in the report
            crReportDocument = rptReport
            crDataBase = crReportDocument.Database
            crTables = crDataBase.Tables
            For Each crTable In crTables
                With crConnInfo
                    .ServerName = p_servername
                    .DatabaseName = p_dbname
                    .UserID = p_username
                    .Password = p_password
                End With
                crTableLogonInfo = crTable.LogOnInfo
                crTableLogonInfo.ConnectionInfo = crConnInfo
                crTable.ApplyLogOnInfo(crTableLogonInfo)
                crTable.ApplyLogOnInfo(crTableLogonInfo)

                crTableLogonInfo = crTable.LogOnInfo
                crTableLogonInfo.ConnectionInfo.ServerName = p_servername
                crTableLogonInfo.ConnectionInfo.DatabaseName = p_dbname
                crTableLogonInfo.ConnectionInfo.UserID = p_username
                crTableLogonInfo.ConnectionInfo.Password = p_password
                crTableLogonInfo.TableName = crTable.Name '// *************See comment below
                crTable.ApplyLogOnInfo(crTableLogonInfo)
                crTable.Location = p_dbname & ".dbo." & crTable.Location.Substring(crTable.Location.LastIndexOf(".") + 1)
            Next
            'Logs into the tables in the Sub-reports
            crSections = crReportDocument.ReportDefinition.Sections
            For Each crSection In crSections
                crReportObjects = crSection.ReportObjects
                For Each crReportObject In crReportObjects
                    If crReportObject.Kind = ReportObjectKind.SubreportObject Then
                        crSubreportObject = CType(crReportObject, SubreportObject)
                        subRepDoc = crSubreportObject.OpenSubreport(crSubreportObject.SubreportName)
                        crDataBase = subRepDoc.Database
                        crTables = crDataBase.Tables
                        For Each crTable In crTables
                            With crConnInfo
                                .ServerName = p_servername
                                .DatabaseName = p_dbname
                                .UserID = p_username
                                .Password = p_password
                            End With
                            crTableLogonInfo = crTable.LogOnInfo
                            crTableLogonInfo.ConnectionInfo = crConnInfo
                            crTable.ApplyLogOnInfo(crTableLogonInfo)
                            crTable.Location = p_dbname & ".dbo." & crTable.Name
                        Next
                    End If
                Next
            Next
            'CrystalReportViewer1.LogOnInfo.Add(crTableLogonInfo)
        Catch err As Exception
            MsgBox(err.Message)
        End Try
    End Sub

    Private Sub CRV_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CRV.Load

    End Sub
End Class