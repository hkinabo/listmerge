﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDIMERGE
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.LIST1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OMRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IMPORTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PREVIEWToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KITUONIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IMPORTToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PREVIEWToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MERGEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GENERATEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LIST1ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LIST2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OMRONLYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KITUONIONLYToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ATTENDANCEDIFFERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EXITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LIST1ToolStripMenuItem, Me.MERGEToolStripMenuItem, Me.REPORTToolStripMenuItem, Me.EXITToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
        Me.MenuStrip.Size = New System.Drawing.Size(843, 28)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'LIST1ToolStripMenuItem
        '
        Me.LIST1ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OMRToolStripMenuItem, Me.KITUONIToolStripMenuItem})
        Me.LIST1ToolStripMenuItem.Name = "LIST1ToolStripMenuItem"
        Me.LIST1ToolStripMenuItem.Size = New System.Drawing.Size(56, 24)
        Me.LIST1ToolStripMenuItem.Text = "LIST1"
        '
        'OMRToolStripMenuItem
        '
        Me.OMRToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IMPORTToolStripMenuItem, Me.PREVIEWToolStripMenuItem})
        Me.OMRToolStripMenuItem.Name = "OMRToolStripMenuItem"
        Me.OMRToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.OMRToolStripMenuItem.Text = "OMR"
        '
        'IMPORTToolStripMenuItem
        '
        Me.IMPORTToolStripMenuItem.Name = "IMPORTToolStripMenuItem"
        Me.IMPORTToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.IMPORTToolStripMenuItem.Text = "IMPORT"
        '
        'PREVIEWToolStripMenuItem
        '
        Me.PREVIEWToolStripMenuItem.Name = "PREVIEWToolStripMenuItem"
        Me.PREVIEWToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.PREVIEWToolStripMenuItem.Text = "PREVIEW"
        '
        'KITUONIToolStripMenuItem
        '
        Me.KITUONIToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IMPORTToolStripMenuItem1, Me.PREVIEWToolStripMenuItem1})
        Me.KITUONIToolStripMenuItem.Name = "KITUONIToolStripMenuItem"
        Me.KITUONIToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.KITUONIToolStripMenuItem.Text = "KITUONI"
        '
        'IMPORTToolStripMenuItem1
        '
        Me.IMPORTToolStripMenuItem1.Name = "IMPORTToolStripMenuItem1"
        Me.IMPORTToolStripMenuItem1.Size = New System.Drawing.Size(152, 24)
        Me.IMPORTToolStripMenuItem1.Text = "IMPORT"
        '
        'PREVIEWToolStripMenuItem1
        '
        Me.PREVIEWToolStripMenuItem1.Name = "PREVIEWToolStripMenuItem1"
        Me.PREVIEWToolStripMenuItem1.Size = New System.Drawing.Size(152, 24)
        Me.PREVIEWToolStripMenuItem1.Text = "PREVIEW"
        '
        'MERGEToolStripMenuItem
        '
        Me.MERGEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GENERATEToolStripMenuItem})
        Me.MERGEToolStripMenuItem.Name = "MERGEToolStripMenuItem"
        Me.MERGEToolStripMenuItem.Size = New System.Drawing.Size(69, 24)
        Me.MERGEToolStripMenuItem.Text = "MERGE"
        '
        'GENERATEToolStripMenuItem
        '
        Me.GENERATEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LIST1ToolStripMenuItem1, Me.LIST2ToolStripMenuItem})
        Me.GENERATEToolStripMenuItem.Name = "GENERATEToolStripMenuItem"
        Me.GENERATEToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.GENERATEToolStripMenuItem.Text = "GENERATE"
        '
        'LIST1ToolStripMenuItem1
        '
        Me.LIST1ToolStripMenuItem1.Name = "LIST1ToolStripMenuItem1"
        Me.LIST1ToolStripMenuItem1.Size = New System.Drawing.Size(152, 24)
        Me.LIST1ToolStripMenuItem1.Text = "LIST1"
        '
        'LIST2ToolStripMenuItem
        '
        Me.LIST2ToolStripMenuItem.Name = "LIST2ToolStripMenuItem"
        Me.LIST2ToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.LIST2ToolStripMenuItem.Text = "LIST2"
        '
        'REPORTToolStripMenuItem
        '
        Me.REPORTToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OMRONLYToolStripMenuItem, Me.KITUONIONLYToolStripMenuItem, Me.ATTENDANCEDIFFERToolStripMenuItem})
        Me.REPORTToolStripMenuItem.Name = "REPORTToolStripMenuItem"
        Me.REPORTToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
        Me.REPORTToolStripMenuItem.Text = "REPORT"
        '
        'OMRONLYToolStripMenuItem
        '
        Me.OMRONLYToolStripMenuItem.Name = "OMRONLYToolStripMenuItem"
        Me.OMRONLYToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.OMRONLYToolStripMenuItem.Text = "OMR ONLY"
        '
        'KITUONIONLYToolStripMenuItem
        '
        Me.KITUONIONLYToolStripMenuItem.Name = "KITUONIONLYToolStripMenuItem"
        Me.KITUONIONLYToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.KITUONIONLYToolStripMenuItem.Text = "KITUONI ONLY"
        '
        'ATTENDANCEDIFFERToolStripMenuItem
        '
        Me.ATTENDANCEDIFFERToolStripMenuItem.Name = "ATTENDANCEDIFFERToolStripMenuItem"
        Me.ATTENDANCEDIFFERToolStripMenuItem.Size = New System.Drawing.Size(221, 24)
        Me.ATTENDANCEDIFFERToolStripMenuItem.Text = "ATTENDANCE DIFFER"
        '
        'EXITToolStripMenuItem
        '
        Me.EXITToolStripMenuItem.Name = "EXITToolStripMenuItem"
        Me.EXITToolStripMenuItem.Size = New System.Drawing.Size(50, 24)
        Me.EXITToolStripMenuItem.Text = "EXIT"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 533)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip.Size = New System.Drawing.Size(843, 25)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(49, 20)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'MDIMERGE
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(843, 558)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "MDIMERGE"
        Me.Text = "MDIParent1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents LIST1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OMRToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KITUONIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MERGEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EXITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IMPORTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PREVIEWToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IMPORTToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PREVIEWToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GENERATEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LIST1ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LIST2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OMRONLYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents KITUONIONLYToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ATTENDANCEDIFFERToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
