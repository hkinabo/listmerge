
create	PROCEDURE [dbo].[cs_ManipulateListOMR]
(
		@RegionNumber varchar(5),
		@DistrictNumber varchar(5),
		@CentreNumber varchar(5),
		@CandidateNumber varchar(5)
      ,@SubjectCode varchar(5)
      ,@SexCode varchar(8)=NULL
      ,@Attendance varchar(5)=1
      ,@TotalMark varchar(8)= NULL
      ,@Item varchar(80)=NULL
	  ,@DeleteFlag INT = null  -- 1 delete
)	
AS 
BEGIN
    SET NOCOUNT ON;	
    MERGE [cs_ListOMR] AS target
    USING 
    (SELECT @RegionNumber,@DistrictNumber,@CentreNumber,@CandidateNumber,@SubjectCode,@SexCode,@Attendance,@TotalMark,@Item,@DeleteFlag) 
    AS source (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item,DeleteFlag)
    ON (
    target.RegionNumber = source.RegionNumber AND
    target.DistrictNumber = source.DistrictNumber  AND
    target.CentreNumber = source.CentreNumber  AND
    target.CandidateNumber = source.CandidateNumber  AND
    target.SubjectCode = source.SubjectCode )
	WHEN MATCHED AND @DeleteFlag = 1 THEN
	  DELETE
    WHEN MATCHED THEN 
	UPDATE
	SET [SexCode] = source.SexCode
      ,[Attendance] = source.Attendance
      ,TotalMark = source.TotalMark
      ,Item = source.Item
     
    WHEN NOT MATCHED and (@DeleteFlag is null or @DeleteFlag=2) THEN
	INSERT (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item)
     VALUES
           (
            source.RegionNumber
           ,source.DistrictNumber
           ,source.CentreNumber
           ,source.CandidateNumber
           ,source.SubjectCode
           ,source.SexCode
           ,source.Attendance
           ,source.TotalMark
           ,source.Item
                      );
END;
GO
