
create VIEW [dbo].[View_centres]
AS
SELECT     a.Id, a.tbl_regions_Id, a.szRegionNumber, a.szRegionName, b.Id AS Centre_id, b.tbl_districts_Id, b.szExamCentreNumber, b.szExamCentreName, a.szDistrictName, a.szDistrictNumber
FROM         dbo.View_region_district AS a INNER JOIN
                      dbo.tbl_prm_exam_centres AS b ON a.Id = b.tbl_districts_Id
GO
