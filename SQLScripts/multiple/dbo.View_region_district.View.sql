
create VIEW [dbo].[View_region_district]
AS
SELECT     a.tbl_regions_Id, b.szRegionNumber, b.szRegionName, a.Id, a.szDistrictNumber, a.szDistrictName
FROM         dbo.tbl_districts AS a INNER JOIN
                      dbo.tbl_regions AS b ON a.tbl_regions_Id = b.Id
GO
