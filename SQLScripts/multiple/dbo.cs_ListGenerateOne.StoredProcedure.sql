
CREATE proc [dbo].[cs_ListGenerateOne]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  c.[RegionNumber]
      ,c.[DistrictNumber]
      ,c.[CentreNumber]
      ,c.[CandidateNumber]
      ,c.[SubjectCode]
      ,c.[SexCode]
      ,case when c.[Attendance]=1 then 'V' else 'A' end Attendance
      ,(cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) TotalMark
      ,c.[Item]
      ,a.[Item1]
      ,a.[Item2]
      ,a.[Item3]
      ,a.[Item4]
      ,a.[Item5]


from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
GO
