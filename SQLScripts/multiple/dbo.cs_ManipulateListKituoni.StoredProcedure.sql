
create	PROCEDURE [dbo].[cs_ManipulateListKituoni]
(
		@RegionNumber varchar(5),
		@DistrictNumber varchar(5),
		@CentreNumber varchar(5),
		@CandidateNumber varchar(5)
      ,@SubjectCode varchar(5)
      ,@SexCode varchar(8)=NULL
      ,@Attendance varchar(5)=1
      ,@TotalMark varchar(8)= NULL
      ,@Item1 varchar(5)=NULL
      ,@Item2 varchar(5)=NULL
      ,@Item3 varchar(5)=NULL
      ,@Item4 varchar(5)=NULL
      ,@Item5 varchar(5)=NULL
	  ,@DeleteFlag INT = null  -- 1 delete
)	
AS 
BEGIN
    SET NOCOUNT ON;	
    MERGE [cs_ListKituoni] AS target
    USING 
    (SELECT @RegionNumber,@DistrictNumber,@CentreNumber,@CandidateNumber,@SubjectCode,@SexCode,@Attendance,@TotalMark,@Item1,@Item2,@Item3,@Item4,@Item5,@DeleteFlag) 
    AS source (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item1,Item2,Item3,Item4,Item5,DeleteFlag)
    ON (
    target.RegionNumber = source.RegionNumber AND
    target.DistrictNumber = source.DistrictNumber  AND
    target.CentreNumber = source.CentreNumber  AND
    target.CandidateNumber = source.CandidateNumber  AND
    target.SubjectCode = source.SubjectCode )
	WHEN MATCHED AND @DeleteFlag = 1 THEN
	  DELETE
    WHEN MATCHED THEN 
	UPDATE
	SET [SexCode] = source.SexCode
      ,[Attendance] = source.Attendance
      ,TotalMark = source.TotalMark
      ,Item1 = source.Item1
      ,Item2 = source.Item2
      ,Item3 = source.Item3
      ,Item4 = source.Item4
      ,Item5 = source.Item5
     
    WHEN NOT MATCHED and (@DeleteFlag is null or @DeleteFlag=2) THEN
	INSERT (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item1,Item2,Item3,Item4,Item5)
     VALUES
           (
            source.RegionNumber
           ,source.DistrictNumber
           ,source.CentreNumber
           ,source.CandidateNumber
           ,source.SubjectCode
           ,source.SexCode
           ,source.Attendance
           ,source.TotalMark
           ,source.Item1
           ,source.Item2
           ,source.Item3
           ,source.Item4
           ,source.Item5
                      );
END;
GO
