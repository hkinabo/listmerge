
CREATE TABLE [dbo].[cs_ListOMR](
	[RegionNumber] [varchar](8) NOT NULL,
	[DistrictNumber] [varchar](5) NOT NULL,
	[CentreNumber] [varchar](5) NOT NULL,
	[CandidateNumber] [varchar](5) NOT NULL,
	[SubjectCode] [varchar](5) NOT NULL,
	[SexCode] [varchar](5) NULL,
	[Attendance] [varchar](5) NULL,
	[TotalMark] [varchar](5) NULL,
	[Item] [varchar](80) NULL,
 CONSTRAINT [PK_cs_ListOMR] PRIMARY KEY CLUSTERED 
(
	[RegionNumber] ASC,
	[DistrictNumber] ASC,
	[CentreNumber] ASC,
	[CandidateNumber] ASC,
	[SubjectCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
