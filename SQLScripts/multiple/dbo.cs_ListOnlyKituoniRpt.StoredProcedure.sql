
CREATE proc [dbo].[cs_ListOnlyKituoniRpt]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS



SELECT  a.[RegionNumber]
      ,a.[DistrictNumber]
      ,a.[CentreNumber]
       ,b.szExamCentreName
      ,a.[CandidateNumber]
      ,a.[SubjectCode]
      ,a.[SexCode]
      ,case when a.[Attendance]=1 then 'V' else 'A' end AttKituoni
 from cs_ListOMR c
right join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber
 
WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
and  c.RegionNumber is null and c.DistrictNumber is null and c.CentreNumber is null and c.CandidateNumber is null
GO

grant exec on [cs_ListOnlyOMRRpt] to public
