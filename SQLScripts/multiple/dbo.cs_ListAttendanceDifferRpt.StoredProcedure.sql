
create proc [dbo].[cs_ListAttendanceDifferRpt]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  c.[RegionNumber]
      ,c.[DistrictNumber]
      ,c.[CentreNumber]
      ,b.szExamCentreName
      ,c.[CandidateNumber]
      ,c.[SubjectCode]
      
      ,case when c.[SexCode]=1 then 'M' else 'F' end SexCode
      ,case when c.[Attendance]=1 then 'V' else 'A' end AttOMR
	  ,cast(c.[TotalMark] as smallint) as TotalMarkOMR
      ,case when a.[Attendance]=1 then 'V' else 'A' end AttKituoni
      ,cast(a.[TotalMark] as smallint) as TotalMarkKituoni
    


from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber




WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber+c.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	c.DistrictNumber	= @DistrictNo AND right(c.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
AND c.Attendance <> a.Attendance
GO
