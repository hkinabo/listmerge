
create proc [dbo].[cs_ListPreviewOMR]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  a.[RegionNumber]
      ,a.[DistrictNumber]
      ,a.[CentreNumber]
      ,a.[CandidateNumber]
      ,a.[SubjectCode]
      ,a.[SexCode]
      ,a.[Attendance]
      ,a.[TotalMark]
      ,a.[Item]
     


from cs_listOMR a
left JOIN View_centres b on (substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
GO
