
create proc [dbo].[cs_ListGenerateTwo]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  
	   c.[CandidateNumber]
	  ,c.[CentreNumber]
      ,c.[DistrictNumber]
      ,c.[RegionNumber]
      
      ,''/*c.[SexCode]*/ SexCode
      ,max(CASE WHEN c.SubjectCode = '01' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS KISW
      ,MAX(CASE WHEN c.SubjectCode = '02' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS ENGL
      ,MAX(CASE WHEN c.SubjectCode = '03' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS MFA
      ,MAX(CASE WHEN c.SubjectCode = '04' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS HISB
      ,MAX(CASE WHEN c.SubjectCode = '05' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS SCE
   
from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1

group by  c.[CandidateNumber]
	  ,c.[CentreNumber]
      ,c.[DistrictNumber]
      ,c.[RegionNumber]
    
      ,c.Attendance
    
    order by 4,3,2,1
GO
