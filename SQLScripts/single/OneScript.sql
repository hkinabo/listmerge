/****** Object:  Table [dbo].[cs_ListOMR]    Script Date: 07/09/2018 12:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cs_ListOMR](
	[RegionNumber] [varchar](8) NOT NULL,
	[DistrictNumber] [varchar](5) NOT NULL,
	[CentreNumber] [varchar](5) NOT NULL,
	[CandidateNumber] [varchar](5) NOT NULL,
	[SubjectCode] [varchar](5) NOT NULL,
	[SexCode] [varchar](5) NULL,
	[Attendance] [varchar](5) NULL,
	[TotalMark] [varchar](5) NULL,
	[Item] [varchar](80) NULL,
 CONSTRAINT [PK_cs_ListOMR] PRIMARY KEY CLUSTERED 
(
	[RegionNumber] ASC,
	[DistrictNumber] ASC,
	[CentreNumber] ASC,
	[CandidateNumber] ASC,
	[SubjectCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cs_ListKituoni]    Script Date: 07/09/2018 12:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cs_ListKituoni](
	[RegionNumber] [varchar](8) NOT NULL,
	[DistrictNumber] [varchar](5) NOT NULL,
	[CentreNumber] [varchar](5) NOT NULL,
	[CandidateNumber] [varchar](5) NOT NULL,
	[SubjectCode] [varchar](5) NOT NULL,
	[SexCode] [varchar](5) NULL,
	[Attendance] [varchar](5) NULL,
	[TotalMark] [varchar](5) NULL,
	[Item1] [varchar](5) NULL,
	[Item2] [varchar](5) NULL,
	[Item3] [varchar](5) NULL,
	[Item4] [varchar](5) NULL,
	[Item5] [varchar](5) NULL,
 CONSTRAINT [PK_cs_ListKituoni] PRIMARY KEY CLUSTERED 
(
	[RegionNumber] ASC,
	[DistrictNumber] ASC,
	[CentreNumber] ASC,
	[CandidateNumber] ASC,
	[SubjectCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[cs_ManipulateListOMR]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create	PROCEDURE [dbo].[cs_ManipulateListOMR]
(
		@RegionNumber varchar(5),
		@DistrictNumber varchar(5),
		@CentreNumber varchar(5),
		@CandidateNumber varchar(5)
      ,@SubjectCode varchar(5)
      ,@SexCode varchar(8)=NULL
      ,@Attendance varchar(5)=1
      ,@TotalMark varchar(8)= NULL
      ,@Item varchar(80)=NULL
	  ,@DeleteFlag INT = null  -- 1 delete
)	
AS 
BEGIN
    SET NOCOUNT ON;	
    MERGE [cs_ListOMR] AS target
    USING 
    (SELECT @RegionNumber,@DistrictNumber,@CentreNumber,@CandidateNumber,@SubjectCode,@SexCode,@Attendance,@TotalMark,@Item,@DeleteFlag) 
    AS source (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item,DeleteFlag)
    ON (
    target.RegionNumber = source.RegionNumber AND
    target.DistrictNumber = source.DistrictNumber  AND
    target.CentreNumber = source.CentreNumber  AND
    target.CandidateNumber = source.CandidateNumber  AND
    target.SubjectCode = source.SubjectCode )
	WHEN MATCHED AND @DeleteFlag = 1 THEN
	  DELETE
    WHEN MATCHED THEN 
	UPDATE
	SET [SexCode] = source.SexCode
      ,[Attendance] = source.Attendance
      ,TotalMark = source.TotalMark
      ,Item = source.Item
     
    WHEN NOT MATCHED and (@DeleteFlag is null or @DeleteFlag=2) THEN
	INSERT (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item)
     VALUES
           (
            source.RegionNumber
           ,source.DistrictNumber
           ,source.CentreNumber
           ,source.CandidateNumber
           ,source.SubjectCode
           ,source.SexCode
           ,source.Attendance
           ,source.TotalMark
           ,source.Item
                      );
END;
GO
/****** Object:  StoredProcedure [dbo].[cs_ManipulateListKituoni]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- grant exec on [cs_ManipulateListOMR] to public
create	PROCEDURE [dbo].[cs_ManipulateListKituoni]
(
		@RegionNumber varchar(5),
		@DistrictNumber varchar(5),
		@CentreNumber varchar(5),
		@CandidateNumber varchar(5)
      ,@SubjectCode varchar(5)
      ,@SexCode varchar(8)=NULL
      ,@Attendance varchar(5)=1
      ,@TotalMark varchar(8)= NULL
      ,@Item1 varchar(5)=NULL
      ,@Item2 varchar(5)=NULL
      ,@Item3 varchar(5)=NULL
      ,@Item4 varchar(5)=NULL
      ,@Item5 varchar(5)=NULL
	  ,@DeleteFlag INT = null  -- 1 delete
)	
AS 
BEGIN
    SET NOCOUNT ON;	
    MERGE [cs_ListKituoni] AS target
    USING 
    (SELECT @RegionNumber,@DistrictNumber,@CentreNumber,@CandidateNumber,@SubjectCode,@SexCode,@Attendance,@TotalMark,@Item1,@Item2,@Item3,@Item4,@Item5,@DeleteFlag) 
    AS source (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item1,Item2,Item3,Item4,Item5,DeleteFlag)
    ON (
    target.RegionNumber = source.RegionNumber AND
    target.DistrictNumber = source.DistrictNumber  AND
    target.CentreNumber = source.CentreNumber  AND
    target.CandidateNumber = source.CandidateNumber  AND
    target.SubjectCode = source.SubjectCode )
	WHEN MATCHED AND @DeleteFlag = 1 THEN
	  DELETE
    WHEN MATCHED THEN 
	UPDATE
	SET [SexCode] = source.SexCode
      ,[Attendance] = source.Attendance
      ,TotalMark = source.TotalMark
      ,Item1 = source.Item1
      ,Item2 = source.Item2
      ,Item3 = source.Item3
      ,Item4 = source.Item4
      ,Item5 = source.Item5
     
    WHEN NOT MATCHED and (@DeleteFlag is null or @DeleteFlag=2) THEN
	INSERT (RegionNumber,DistrictNumber,CentreNumber,CandidateNumber,SubjectCode,SexCode,Attendance,TotalMark,Item1,Item2,Item3,Item4,Item5)
     VALUES
           (
            source.RegionNumber
           ,source.DistrictNumber
           ,source.CentreNumber
           ,source.CandidateNumber
           ,source.SubjectCode
           ,source.SexCode
           ,source.Attendance
           ,source.TotalMark
           ,source.Item1
           ,source.Item2
           ,source.Item3
           ,source.Item4
           ,source.Item5
                      );
END;
GO
/****** Object:  View [dbo].[View_region_district]    Script Date: 07/09/2018 12:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_region_district]
AS
SELECT     a.tbl_regions_Id, b.szRegionNumber, b.szRegionName, a.Id, a.szDistrictNumber, a.szDistrictName
FROM         dbo.tbl_districts AS a INNER JOIN
                      dbo.tbl_regions AS b ON a.tbl_regions_Id = b.Id
GO

/****** Object:  View [dbo].[View_centres]    Script Date: 07/09/2018 12:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_centres]
AS
SELECT     a.Id, a.tbl_regions_Id, a.szRegionNumber, a.szRegionName, b.Id AS Centre_id, b.tbl_districts_Id, b.szExamCentreNumber, b.szExamCentreName, a.szDistrictName, a.szDistrictNumber
FROM         dbo.View_region_district AS a INNER JOIN
                      dbo.tbl_prm_exam_centres AS b ON a.Id = b.tbl_districts_Id
GO

/****** Object:  StoredProcedure [dbo].[cs_ListPreviewOMR]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--grant exec on cs_ListPreviewKituoni to public
create proc [dbo].[cs_ListPreviewOMR]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  a.[RegionNumber]
      ,a.[DistrictNumber]
      ,a.[CentreNumber]
      ,a.[CandidateNumber]
      ,a.[SubjectCode]
      ,a.[SexCode]
      ,a.[Attendance]
      ,a.[TotalMark]
      ,a.[Item]
     


from cs_listOMR a
left JOIN View_centres b on (substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
GO
/****** Object:  StoredProcedure [dbo].[cs_ListPreviewKituoni]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[cs_ListPreviewKituoni]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  a.[RegionNumber]
      ,a.[DistrictNumber]
      ,a.[CentreNumber]
      ,a.[CandidateNumber]
      ,a.[SubjectCode]
      ,a.[SexCode]
      ,a.[Attendance]
      ,a.[TotalMark]
      ,a.[Item1]
      ,a.[Item2]
      ,a.[Item3]
      ,a.[Item4]
      ,a.[Item5]


from cs_listkituoni a
left JOIN View_centres b on (substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
GO
/****** Object:  StoredProcedure [dbo].[cs_ListOnlyOMRRpt]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[cs_ListOnlyOMRRpt]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS



SELECT  c.[RegionNumber]
      ,c.[DistrictNumber]
      ,c.[CentreNumber]
       ,b.szExamCentreName
      ,c.[CandidateNumber]
      ,c.[SubjectCode]
      ,case when c.[SexCode]=1 then 'M' else 'F' end SexCode
      ,case when c.[Attendance]=1 then 'V' else 'A' end AttOMR
      

from cs_ListOMR c
LEFT join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber
 


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber+c.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(c.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	c.DistrictNumber	= @DistrictNo AND right(c.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
and  a.RegionNumber is null and a.DistrictNumber is null and a.CentreNumber is null and a.CandidateNumber is null
GO
/****** Object:  StoredProcedure [dbo].[cs_ListOnlyKituoniRpt]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--grant exec on [cs_ListOnlyOMRRpt] to public
CREATE proc [dbo].[cs_ListOnlyKituoniRpt]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS



SELECT  a.[RegionNumber]
      ,a.[DistrictNumber]
      ,a.[CentreNumber]
       ,b.szExamCentreName
      ,a.[CandidateNumber]
      ,a.[SubjectCode]
      ,a.[SexCode]
      ,case when a.[Attendance]=1 then 'V' else 'A' end AttKituoni
 from cs_ListOMR c
right join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber
 
WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
and  c.RegionNumber is null and c.DistrictNumber is null and c.CentreNumber is null and c.CandidateNumber is null
GO
/****** Object:  StoredProcedure [dbo].[cs_ListGenerateTwo]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[cs_ListGenerateTwo]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  
	   c.[CandidateNumber]
	  ,c.[CentreNumber]
      ,c.[DistrictNumber]
      ,c.[RegionNumber]
      
      ,''/*c.[SexCode]*/ SexCode
      ,max(CASE WHEN c.SubjectCode = '01' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS KISW
      ,MAX(CASE WHEN c.SubjectCode = '02' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS ENGL
      ,MAX(CASE WHEN c.SubjectCode = '03' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS MFA
      ,MAX(CASE WHEN c.SubjectCode = '04' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS HISB
      ,MAX(CASE WHEN c.SubjectCode = '05' THEN (CASE WHEN  (c.Item is null and (c.Attendance is null or c.Attendance = '0' or c.Attendance='1')) THEN 0 ELSE (cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) END) END)  AS SCE
   
from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1

group by  c.[CandidateNumber]
	  ,c.[CentreNumber]
      ,c.[DistrictNumber]
      ,c.[RegionNumber]
    
      ,c.Attendance
    
    order by 4,3,2,1
GO
/****** Object:  StoredProcedure [dbo].[cs_ListGenerateOne]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[cs_ListGenerateOne]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  c.[RegionNumber]
      ,c.[DistrictNumber]
      ,c.[CentreNumber]
      ,c.[CandidateNumber]
      ,c.[SubjectCode]
      ,c.[SexCode]
      ,case when c.[Attendance]=1 then 'V' else 'A' end Attendance
      ,(cast(a.[TotalMark] as smallint)+cast(c.TotalMark as smallint)) TotalMark
      ,c.[Item]
      ,a.[Item1]
      ,a.[Item2]
      ,a.[Item3]
      ,a.[Item4]
      ,a.[Item5]


from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber


WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber+a.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	a.DistrictNumber	= @DistrictNo AND right(a.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(a.RegionNumber,3,2)+a.DistrictNumber+a.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
GO
/****** Object:  StoredProcedure [dbo].[cs_ListAttendanceDifferRpt]    Script Date: 07/09/2018 12:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--grant exec on [cs_ListSexDifferRpt] to public
create proc [dbo].[cs_ListAttendanceDifferRpt]
(
	@candidate_no_from VARCHAR(32)=NULL,
	@candidate_no_to VARCHAR(32)=NULL,
	@ReportLevel VARCHAR(50),
	@RegionNumber VARCHAR(32) = NULL,
	@DistrictNo VARCHAR(32) = NULL	,
	@CentreNo VARCHAR(32) = null
)
AS

SELECT  c.[RegionNumber]
      ,c.[DistrictNumber]
      ,c.[CentreNumber]
      ,b.szExamCentreName
      ,c.[CandidateNumber]
      ,c.[SubjectCode]
      
      ,case when c.[SexCode]=1 then 'M' else 'F' end SexCode
      ,case when c.[Attendance]=1 then 'V' else 'A' end AttOMR
	  ,cast(c.[TotalMark] as smallint) as TotalMarkOMR
      ,case when a.[Attendance]=1 then 'V' else 'A' end AttKituoni
      ,cast(a.[TotalMark] as smallint) as TotalMarkKituoni
    


from cs_ListOMR c
inner join cs_listkituoni a  on a.RegionNumber=c.RegionNumber and a.DistrictNumber=c.DistrictNumber
and a.CentreNumber=c.CentreNumber and a.CandidateNumber=c.CandidateNumber and a.SubjectCode=c.SubjectCode
left JOIN View_centres b on (substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)=b.szExamCentreNumber
LEFT JOIN View_region_district d on b.szDistrictNumber= d.szDistrictNumber AND b.szRegionNumber= d.szRegionNumber




WHERE 
	(
			CASE 
				WHEN (@ReportLevel = 'Candidate' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber+c.CandidateNumber) BETWEEN @candidate_no_from AND @candidate_no_to) THEN 1 
				WHEN (@ReportLevel = 'Region' AND	right(a.RegionNumber,2)	= @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'District' AND	c.DistrictNumber	= @DistrictNo AND right(c.RegionNumber,2) = @RegionNumber) THEN 1 
				WHEN (@ReportLevel = 'Centre' AND	(substring(c.RegionNumber,3,2)+c.DistrictNumber+c.CentreNumber)		= @CentreNo) THEN 1 
				ELSE 0
			END
		  ) = 1
AND c.Attendance <> a.Attendance
GO
